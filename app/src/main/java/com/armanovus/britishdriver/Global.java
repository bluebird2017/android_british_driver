package com.armanovus.britishdriver;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import com.armanovus.britishdriver.models.Manifest;
import com.armanovus.britishdriver.models.NoteType;
import com.armanovus.britishdriver.models.response.LoginResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Global {
    public static int CURRENT_POINT_INDEX = 0;
    public static int SELECTED_ITEM_INDEX = 0;
    public static LoginResponse DriverData;
    public static List<NoteType> NoteTypes;

    public static Location GetLocation(Context context) {
        String locationProvider = LocationManager.GPS_PROVIDER;
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
        return lastKnownLocation;
    }

    public static void loadNotes() {
        String jsonNotes = "[{\"Id\":2,\"n\":\"Emergency\",\"mc\":false,\"kpi\":false},{\"Id\":3,\"n\":\"Security\",\"mc\":false,\"kpi\":false},{\"Id\":4,\"n\":\"Accident\",\"mc\":false,\"kpi\":false},{\"Id\":5,\"n\":\"Keluar Rute\",\"mc\":false,\"kpi\":false},{\"Id\":6,\"n\":\"Chap Replacement\",\"mc\":false,\"kpi\":false},{\"Id\":7,\"n\":\"Storing\",\"mc\":false,\"kpi\":false},{\"Id\":8,\"n\":\"Others\",\"mc\":false,\"kpi\":false},{\"Id\":9,\"n\":\"Finish\",\"mc\":false,\"kpi\":false}]";
        Gson gson = new Gson();
        List<NoteType> noteTypes = gson.fromJson(jsonNotes, new TypeToken<List<NoteType>>(){}.getType());
        NoteTypes = noteTypes;
    }
}
