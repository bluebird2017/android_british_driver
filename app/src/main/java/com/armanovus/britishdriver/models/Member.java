package com.armanovus.britishdriver.models;

public class Member
{
    public long Id;
    public String FirstName;
    public String MiddleName;
    public String Surname;
    public String HomePhone;
    public String GroupName;
    public String AdditionalInfo;
    public String Remark;
    public boolean IsRegular;

    public boolean HasMedicalRecord;
    public long SiteId;
    //public Site Site;

//    public List<MemberActivity> MemberActivities;
//    public List<Address> Addresses;
//    public List<MemberPreference> Preferences;
}