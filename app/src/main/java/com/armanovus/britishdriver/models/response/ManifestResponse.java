package com.armanovus.britishdriver.models.response;

import com.armanovus.britishdriver.models.Manifest;
import java.util.List;

public class ManifestResponse
{
    public List<Manifest> Manifests;
}