package com.armanovus.britishdriver.models.param;

import com.google.gson.annotations.SerializedName;

public class ManifestItemCheckinParam extends CheckinBaseParam {
    @SerializedName("miid")
    public long ManifestItemId;
}
