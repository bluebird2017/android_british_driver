package com.armanovus.britishdriver.models;

import com.armanovus.britishdriver.Constant;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Manifest
{
	public long Id;

	@SerializedName("n")
	public String Name;

	@SerializedName("md")
	public Date ManifestDate;

	public String getRouteFullName()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("ddd, dd MMM");
		String time = sdf.format(ManifestDate);
		return String.format("Route %s-%s-%s", Name, Constant.getRouteType(RouteType), time);
	}

	public String getChaperone()
	{
		return String.format("Chaperone: %s", ChaperoneName);
	}

	@SerializedName("rt")
	public Date ReleaseTime;
	
	@SerializedName("ot")
	public Date OpenTime;

	@SerializedName("st")
	public Date StartTime;
	@SerializedName("ft")
	public Date FinishTime;
	@SerializedName("ct")
	public Date CloseTime;

	@SerializedName("fi")
	public long FleetId;
	
	public Fleet Fleet;

	@SerializedName("chid")
	//[Column("ChapId")
	public long ChaperoneId;

	@SerializedName("chc")
	public String ChaperoneCode;

	@SerializedName("chn")
	//[Column("ChapName")
	public String ChaperoneName;

	@SerializedName("chg")
	//[Column("ChapGender")
	public String ChaperoneGender;

	@SerializedName("chpa")
	//[Column("ChapPickupAddr")
	public String ChaperonePickupAddress;

	@SerializedName("chlat")
	//[Column("ChapLat")
	public double ChaperoneLatitude;

	@SerializedName("chlon")
	//[Column("ChapLon")
	public double ChaperoneLongitude;

	@SerializedName("chpt")
	//[Column("ChapPickupTime")
	public Date ChaperonePickupTime;

	//@SerializedName("chct")
	public Date ChaperoneCheckinTime;

	@SerializedName("chont")
	public Date ChaperoneOnboardTime;

	//@SerializedName("choft")
	public Date ChaperoneOffboardTime;

	//@SerializedName("chskpt")
	public Date ChaperoneSkipTime;
	
	public String getChaperonePickup()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
		String time = sdf.format(ChaperonePickupTime);
		return String.format("Pickup Time: %s at %s", ChaperonePickupTime == null ? "" : time, ChaperonePickupAddress);
	}

	@SerializedName("tacd")
	//[Column("TAppClientId")
	public String TransporterAppClientId;

	@SerializedName("dti")
	//[Column("DriverTId")
	public String DriverTransporterId;

	@SerializedName("dn")
	public String DriverName;

	@SerializedName("lp")
	public String LicensePlate;

	@SerializedName("fc")
	public String FleetCode;

	@SerializedName("fty")
	public String FleetType;

	@SerializedName("pc")
	public String PoolCode;

	@SerializedName("pn")
	public String PoolName;

	
	//[Column("PermitId")
	public long OperationPermitId;

	//public  OperationPermit OperationPermit;
	
	public String PermitCode;
	
	public String PermitYear;
	
	public String PermitFleetCode;
	
	public String PermitPoolCode;
	
	public String PermitPoolName;
	
	public String PermitDriverCode;
	
	public String PermitDriverName;
	
	public Date PermissionDate;

	@SerializedName("rty")
	public int RouteType;

	@SerializedName("tid")
	public long TermId;
	
	public Term Term;

	
	public Date DeleteDate;

	@SerializedName("cc")
	public String CustomerCode;

	@SerializedName("mits")
	public List<ManifestItem> ManifestItems;

	public List<ManifestItemDetail> ListStudents;

	//public boolean IsChaperoneCheckin;
	public boolean IsCheckin;

	public boolean checkIfLastPoint(int currentPointIndex, ManifestItem gateItems) {
		boolean isLast = true;
		for (int i = currentPointIndex; i < ManifestItems.size(); i++) {
			if(!ManifestItems.get(i).checkAllSkipPM(gateItems))
			{
				isLast = false;
				break;
			}
		}

		return  isLast;
	}
}

//enum RouteType
//{
//	AM,PM
//}