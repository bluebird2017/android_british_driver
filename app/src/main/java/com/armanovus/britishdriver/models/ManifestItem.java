package com.armanovus.britishdriver.models;

import android.text.TextUtils;

import com.armanovus.britishdriver.Constant;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ManifestItem {
	public int No;

        public boolean checkIsGate()
        {
            return !TextUtils.isEmpty(AddressAlias) && AddressAlias.toUpperCase().contains("BSJ GATE") ? true : false;
        }        

        public long Id;

        @SerializedName("aals")
        public String AddressAlias;

        @SerializedName("adtl")
        private String AddressDetail;

        public String getAddressDetail()
        {
            return TextUtils.isEmpty(AddressDetail) ? AddressAlias : AddressDetail;
        }

        @SerializedName("desc")
        public String Description;
        @SerializedName("lat")
        public double Latitude;
        @SerializedName("lon")
        public double Longitude;
        @SerializedName("aid")
        public long AddressId;
        @SerializedName("mid")
        public long ManifestId;
        @SerializedName("pdt")
        public Date PickupTime;

        public String getPickupTime()
        {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
            return sdf.format(PickupTime);
        }
        
        public Address Address;
        public Manifest Manifest;

        @SerializedName("mds")
        public List<ManifestItemDetail> ManifestItemDetails;

//        private boolean _isSkip;
//        public boolean IsSkip
//        {
//            get
//            {
//                return _isSkip;
//            }
//            set
//            {
//                TryChangeProperty(ref _isSkip, value, "IsSkip");
//            }
//        }

        public boolean isSkip;

//        private bool _isCheckin;
//        public bool IsCheckin
//        {
//            get
//            {
//                return _isCheckin;
//            }
//            set
//            {
//                TryChangeProperty(ref _isCheckin, value, "IsCheckin");
//            }
//        }

        @SerializedName("typ")
        public int ManifestItemType;

    public int getAbsentCount() {
        int count = 0;
        for (int i = 0; i < ManifestItemDetails.size(); i++) {
            if(ManifestItemDetails.get(i).SkipTime != null || ManifestItemDetails.get(i).PlannedSkipTime != null)
                count++;
        }
        return count;
    }

    public int getPresentCount() {
        int count = 0;
        for (int i = 0; i < ManifestItemDetails.size(); i++) {
            if(ManifestItemDetails.get(i).PickDropTime != null && ManifestItemDetails.get(i).PlannedSkipTime == null)
                count++;
        }
        return count;
    }

    public boolean checkAllSkip() {
        boolean isAllSkip = true;
        for (int i = 0; i < ManifestItemDetails.size(); i++) {
            if(ManifestItemDetails.get(i).PlannedSkipTime == null && ManifestItemDetails.get(i).DriverPlannedSkipTime == null)
            {
                isAllSkip = false;
                break;
            }
        }

        return isAllSkip;
    }
    
    public boolean checkAllSkipPM(ManifestItem gateItem) {
        boolean isAllSkip = true;
        for (int i = 0; i < ManifestItemDetails.size(); i++) {
            for (int j = 0; j < gateItem.ManifestItemDetails.size(); j++) {
                if(gateItem.ManifestItemDetails.get(j).MemberId == ManifestItemDetails.get(i).MemberId) {
                    if (ManifestItemDetails.get(i).PlannedSkipTime == null && ManifestItemDetails.get(i).DriverPlannedSkipTime == null && gateItem.ManifestItemDetails.get(j).SkipTime == null) {
                        isAllSkip = false;
                        break;
                    }
                }
            }
        }

        return isAllSkip;
    }


//        public void CheckinAll(bool isCheckin)
//        {
//            for (int i = 0; i < ManifestItemDetails.Count; i++)
//            {
//                ManifestItemDetails[i].IsCheckin = true;
//            }
//        }
    }

    enum ManifestItemType
    {
        Pick,
        Drop,
        Checkpoint,
        Split
    }