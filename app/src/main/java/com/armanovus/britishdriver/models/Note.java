package com.armanovus.britishdriver.models;

import com.google.gson.annotations.SerializedName;

public class Note
{
	@SerializedName("ntypid")
	public int NoteTypeId;

	@SerializedName("mid")
	public long ManifestId;

	@SerializedName("dtls")
	public String Details;

	@SerializedName("con")
	public String Contributor;
}