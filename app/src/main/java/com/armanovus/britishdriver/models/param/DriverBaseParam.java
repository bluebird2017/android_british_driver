package com.armanovus.britishdriver.models.param;

import com.armanovus.britishdriver.util.Utility;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DriverBaseParam {
    @SerializedName("pdt")
    public String PhoneDateTime;

    @SerializedName("un")
    public String Username;

    @SerializedName("did")
    public String DeviceId;

    public static String HARDWARE_ID;

    public DriverBaseParam()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ"); // This should work for you. Though I must say 6 "S" is not done. You won't get milliseconds for 6 precisions.
        String dateString = dateFormat.format(Utility.getDate());
        PhoneDateTime = dateString;
        DeviceId = HARDWARE_ID;
    }
}
