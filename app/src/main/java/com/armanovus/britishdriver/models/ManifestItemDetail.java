package com.armanovus.britishdriver.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ManifestItemDetail
{
    public long Id;

    @SerializedName("mid")
    public long MemberId;

    public Member Member;

    @SerializedName("mn")
    public String MemberName;
    @SerializedName("mdtl")
    public String MemberDetail;
    @SerializedName("inst")
    public String Instruction;


    public long ManifestItemId;
    public int ManifestItemType;

    private Date _pickDropTime;

    @SerializedName("mpdt")
    public Date PickDropTime;

    private Date _skipTime;
    @SerializedName("skpt")
    public Date SkipTime;

    public boolean isOnboard;

    private Date _plannedSkipTime;
    @SerializedName("pskpt")
    public Date PlannedSkipTime;

    private Date _driverPlannedSkipTime;

    public Date DriverPlannedSkipTime;

    private boolean _isCheckin;;

    @SerializedName("pskpn")
    public String PlannedSkipNote;

    //public ManifestItem ManifestItem;
    public boolean isSkip;
    public boolean getIsSkip()
    {
        if (getIsPlannedSkip()) return true;

        if (!_isCheckin) return false;

        if (PlannedSkipTime != null) return true;

        if (PickDropTime != null && SkipTime != null)
        {
            if (PickDropTime.compareTo(SkipTime) > 0)
                return false;
            else
                return true;
        }
        else if (SkipTime == null)
            return false;
        else
            return true;
    }

    private boolean _isPlannedSkip;
    public boolean getIsPlannedSkip()
    {
        return PlannedSkipTime != null || DriverPlannedSkipTime != null ? true : _isPlannedSkip;
    }


    public boolean getIsServerPlannedSkip()
    {
        return PlannedSkipTime != null ? true : false;
    }


    //private bool _isSkip;
    public boolean getIsPresent()
    {
        if (!_isCheckin) return false;

        if (PickDropTime != null && SkipTime != null)
        {
            if (PickDropTime.compareTo(SkipTime) > 0)
                return true;
            else
                return false;
        }
        else if (PickDropTime == null)
            return false;
        else
            return true;
    }

    private boolean _isInitialized;
    public boolean getIsInitialized()
    {
        if (SkipTime != null) return true;

        return _isInitialized;
    }

    private boolean _isRemoved;
    public boolean getIsRemoved()
    {
        return _isRemoved;
    }
}