package com.armanovus.britishdriver.models;

import com.google.gson.annotations.SerializedName;

public class AppConfig
{
	public long Id;

	@SerializedName("k")
	public String Key;

	@SerializedName("vd")
	public String ValueData;

	@SerializedName("cg")
	public String ConfigGroup;
}