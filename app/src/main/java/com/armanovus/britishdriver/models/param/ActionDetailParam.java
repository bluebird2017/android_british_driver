package com.armanovus.britishdriver.models.param;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class ActionDetailParam
{
    @SerializedName("did")
    public long DetailId;
    @SerializedName("aty")
    public int ActionType;
    @SerializedName("st")
    public Date SkipTime;
    @SerializedName("at")
    public Date ActionTime;
}


