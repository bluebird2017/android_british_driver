package com.armanovus.britishdriver.models.response;

import com.armanovus.britishdriver.models.Manifest;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse extends BaseResponse
{
//    @SerializedName("tid")
//    public int TermId;

    @SerializedName("pc")
    public String PermitCode;

    @SerializedName("py")
    public String PermitYear;

    @SerializedName("fc")
    public String FleetCode;

    @SerializedName("poc")
    public String PoolCode;

    @SerializedName("nip")
    public String DriverCode;

    @SerializedName("dn")
    public String DriverName;

    @SerializedName("adid")
    public long ActiveDriverId;

    public String Username;
}