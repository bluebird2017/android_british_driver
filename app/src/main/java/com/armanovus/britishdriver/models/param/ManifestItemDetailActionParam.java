package com.armanovus.britishdriver.models.param;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class ManifestItemDetailActionParam extends CheckinBaseParam {
    @SerializedName("dtls")
    public List<ActionDetailParam> ActionDetails;

    @SerializedName("miid")
    public long ManifestItemId;
}