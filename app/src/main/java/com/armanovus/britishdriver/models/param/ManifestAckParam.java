package com.armanovus.britishdriver.models.param;

import com.google.gson.annotations.SerializedName;

public class ManifestAckParam extends DriverBaseParam {
    @SerializedName("mid")
    public long ManifestId;

    @SerializedName("adid")
    public long ActiveDriverId;
}
