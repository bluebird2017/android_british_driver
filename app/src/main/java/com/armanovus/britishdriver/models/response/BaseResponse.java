package com.armanovus.britishdriver.models.response;

import com.google.gson.annotations.SerializedName;

public class BaseResponse
{
    @SerializedName("ie")
    public boolean IsError;
    @SerializedName("ec")
    public String ErrorCode;
    @SerializedName("em")
    public String ErrorMessage;

//    @SerializedName("v")
//    public AppConfig Version;
//    @SerializedName("p")
//    public AppConfig Path;
}