package com.armanovus.britishdriver.models.param;

import com.google.gson.annotations.SerializedName;

public class CheckinBaseParam extends DriverBaseParam {
    @SerializedName("mid")
    public long ManifestId;
    @SerializedName("lat")
    public double Latitude;
    @SerializedName("lon")
    public double Longitude;

    @SerializedName("adid")
    public long ActiveDriverId;

    @SerializedName("ts")
    public int TrackingStatus;
}
