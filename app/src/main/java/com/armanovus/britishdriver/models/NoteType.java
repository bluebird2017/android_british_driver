package com.armanovus.britishdriver.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class NoteType
{
	@SerializedName("Id")
	public int Id;

	@SerializedName("n")
	public String Name;

	@SerializedName("mc")
	public boolean MustClose;
}