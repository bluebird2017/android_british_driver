package com.armanovus.britishdriver.models;

public class Address
{
        public long Id;
        public String Alias;
        public double Latitude;
        public double Longitude;
        //public string Detail;
        private String _detail;

        public String getDetail()
        {
                return MemberId == 0 || MemberId == 0 ? Alias : _detail;
        }
        
        public String PostalCode;
        public String City;
        
        public long MemberId;

        public Member Member;
}