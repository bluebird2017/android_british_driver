package com.armanovus.britishdriver.models;

import java.util.Date;

public class Term
{
	public long Id;
	public String Name;
	public int TermYear;
	public int TermPart;
	public Date StartDate;
	public Date EndDate;

	//public List<MemberPreference> MemberPreferences;
}