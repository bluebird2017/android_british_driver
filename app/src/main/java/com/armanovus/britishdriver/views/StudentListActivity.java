package com.armanovus.britishdriver.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.armanovus.britishdriver.Constant;
import com.armanovus.britishdriver.Global;
import com.armanovus.britishdriver.R;
import com.armanovus.britishdriver.fragments.ListPointFragment;
import com.armanovus.britishdriver.fragments.ListStudentFragment;
import com.armanovus.britishdriver.listener.ResponseListener;
import com.armanovus.britishdriver.models.ApiParam;
import com.armanovus.britishdriver.models.Manifest;
import com.armanovus.britishdriver.models.ManifestItem;
import com.armanovus.britishdriver.models.ManifestItemDetail;
import com.armanovus.britishdriver.models.param.CheckinBaseParam;
import com.armanovus.britishdriver.models.param.DriverBaseParam;
import com.armanovus.britishdriver.models.response.LoginResponse;
import com.armanovus.britishdriver.util.Utility;
import com.armanovus.britishdriver.util.VolleyHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class StudentListActivity extends AppCompatActivity implements ResponseListener {
    private final int REQUEST_MANIFEST = 1;
    private final int REQUEST_MANIFEST_START = 2;

    @Override
    public void onResponse(String response, int requestCode) {
        if(requestCode == REQUEST_MANIFEST)
        {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
            List<Manifest> manifests = gson.fromJson(response, new TypeToken<List<Manifest>>(){}.getType());

            if(manifests == null || manifests.size() == 0)
            {
                //showProgress(false);
                Utility.showAlert(this, "Tidak ada order.");
                return;
            }

            Manifest manifest = manifests.get(0);
            manifest.ListStudents = new ArrayList<>();
            for (int i = 0; i < manifest.ManifestItems.size(); i++) {
                ManifestItem mi = manifest.ManifestItems.get(i);
                if (manifest.RouteType == 0 && mi.ManifestItemType == Constant.MANIFEST_ITEM_TYPE_CHECKPOINT)
                    break;

                if (manifest.RouteType == 1 && mi.ManifestItemType == Constant.MANIFEST_ITEM_TYPE_PICKUP)
                {
                    continue;
                }

                //if (Manifest.RouteType == RouteType.AM && item.ManifestItemDetails.Count == 0)
                //    break;
                for (int j = 0; j < mi.ManifestItemDetails.size(); j++) {
                    mi.ManifestItemDetails.get(j).ManifestItemId = mi.Id;
                    manifest.ListStudents.add(mi.ManifestItemDetails.get(j));
                }
            }

            Collections.sort(manifest.ListStudents, new Comparator<ManifestItemDetail>() {
                @Override
                public int compare(ManifestItemDetail o1, ManifestItemDetail o2) {
                    return o1.MemberName.toUpperCase().compareTo(o2.MemberName.toUpperCase());
                }
            });
            Constant.Manifests = manifests;
            listPointFragment.refresh(manifests.get(0));
            listStudentFragment.refresh(manifests.get(0));
            mProgressView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorResponse(int requestCode) {
        mProgressView.setVisibility(View.GONE);
    }

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private ListPointFragment listPointFragment;
    private ListStudentFragment listStudentFragment;
    private Button btnStart;
    private View mProgressView;
    private RequestQueue queue;
    private Handler handler;
    private Runnable runnable;
    private Manifest manifest;
    private boolean isOncreating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mProgressView = findViewById(R.id.login_progress);

        manifest = Constant.Manifests.get(0);
        TextView txtRoute = (TextView) findViewById(R.id.txt_route_name);
        txtRoute.setText(manifest.getRouteFullName());

        TextView txtChaperoneName = (TextView) findViewById(R.id.txt_chaperone_name);
        txtChaperoneName.setText(manifest.getChaperone());

        TextView txtChaperonePickup = (TextView) findViewById(R.id.txt_chaperone_pickup);
        txtChaperonePickup.setText(manifest.getChaperonePickup());

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        listPointFragment = new ListPointFragment();
        listStudentFragment = new ListStudentFragment();

        btnStart = (Button) findViewById(R.id.btn_start);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(StudentListActivity.this, R.style.CustomDialog);
                // Add the buttons
                builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        manifestStart();
                        Global.CURRENT_POINT_INDEX = 0;
//                if(manifest.ChaperoneOnboardTime != null)
//                    Global.CURRENT_POINT_INDEX++;
                        if(manifest.RouteType == 0) { // AM
                            if(Global.CURRENT_POINT_INDEX > 0) {
                                while (Global.CURRENT_POINT_INDEX < manifest.ManifestItems.size() && manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX - 1).isSkip) {
                                    Global.CURRENT_POINT_INDEX++;
                                }
                            }
                            Intent intent = new Intent(StudentListActivity.this, OrderActivity.class);
                            startActivity(intent);
                        }
                        else // PM
                        {
                            Intent intent = new Intent(StudentListActivity.this, OrderPMActivity.class);
                            startActivity(intent);
                        }
                    }
                });

                builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

                builder.setCancelable(false);
                builder.setMessage("Anda yakin akan memulai perjalanan?")
                        .setTitle("Pesan");

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        queue = Volley.newRequestQueue(StudentListActivity.this);
        //loadListOrder();

        handler = new Handler();
        handler.postDelayed(runnable, 10000);
        runnable = new Runnable() {
            @Override
            public void run() {
                ping();
                handler.postDelayed(this, 10000);
            }
        };

        runnable.run();
        isOncreating = true;
    }

    public void manifestStart()
    {
        String url = Constant.BASE_URL + Constant.MANIFEST_START_API;
        Location loc = Global.GetLocation(this);

        CheckinBaseParam objParam = new CheckinBaseParam();
        objParam.ActiveDriverId = Global.DriverData.ActiveDriverId;
        objParam.Username = Global.DriverData.Username;
        objParam.Latitude = 0;
        objParam.Longitude = 0;
        if(loc != null)
        {
            objParam.Latitude = loc.getLatitude();
            objParam.Longitude = loc.getLongitude();
        }
        objParam.ManifestId = manifest.Id;
        //objParam.TrackingStatus = 2;
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ").create();
        final String params = gson.toJson(objParam);
        //VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_MANIFEST_START, false);
        ApiParam apiParam = new ApiParam();
        apiParam.Url = url;
        apiParam.Params = params;
        VolleyHelper.getInstance(this).addApiParam(apiParam);
    }

    public void ping()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ"); // This should work for you. Though I must say 6 "S" is not done. You won't get milliseconds for 6 precisions.
        Date date = new Date();
        date.setDate(14);
        String dateString = dateFormat.format(date);
        String url = Constant.BASE_URL + Constant.PING_API + "?nip=" + Global.DriverData.Username + "&" + dateString;
        VolleyHelper.getInstance(this).getResponse(url, this, -1, false);
        VolleyHelper.getInstance(this).sendRequest();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_student_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            reloadData();
        }

        return super.onOptionsItemSelected(item);
    }

    private void reloadData() {
        mProgressView.setVisibility(View.VISIBLE);;
        String url = Constant.BASE_URL + Constant.DOWNLOAD_MANIFEST_API;
        JSONObject objParam = new JSONObject();
        try {
            objParam.put("un", Global.DriverData.DriverCode);
            objParam.put("tid", 0);
            objParam.put("pc", Global.DriverData.PermitCode);
            objParam.put("py", Global.DriverData.PermitYear);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ"); // This should work for you. Though I must say 6 "S" is not done. You won't get milliseconds for 6 precisions.
                        String dateString = dateFormat.format(Utility.getDate());
            objParam.put("pdt", dateString);
            objParam.put("od", dateString);
            objParam.put("did", DriverBaseParam.HARDWARE_ID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String params = objParam.toString();
        VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_MANIFEST, true);
    }

    public void skipManifestItem(ManifestItem item) {
        for (int j = 0; j < Constant.Manifests.get(0).ListStudents.size(); j++) {
            if(Constant.Manifests.get(0).ListStudents.get(j).ManifestItemId == item.Id) {
                Constant.Manifests.get(0).ListStudents.get(j).isSkip = item.isSkip;
                Constant.Manifests.get(0).ListStudents.get(j).SkipTime = item.isSkip ? new Date() : null;
            }
        }
        listStudentFragment.refresh();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_point, container, false);
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            if(position == 0) {
                //newOrderFragment = new NewOrderFragment();
                return listPointFragment;
            }
            else if(position == 1){
                //activeOrderFragment = new ActiveOrderFragment();
                return listStudentFragment;
            }
            else
                return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "DAFTAR JEMPUT";
                case 1:
                    return "DAFTAR SISWA";
            }
            return null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        if(!isOncreating) {
            listStudentFragment.refresh();
        }
        else
            isOncreating = false;
    }

}
