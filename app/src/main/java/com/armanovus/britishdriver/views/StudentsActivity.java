package com.armanovus.britishdriver.views;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DimenRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.armanovus.britishdriver.Constant;
import com.armanovus.britishdriver.Global;
import com.armanovus.britishdriver.R;
import com.armanovus.britishdriver.models.Manifest;
import com.armanovus.britishdriver.models.ManifestItem;
import com.armanovus.britishdriver.models.ManifestItemDetail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StudentsActivity extends AppCompatActivity {
    private Button btnStart;
    private View mProgressView;
    private RequestQueue queue;
    private Handler handler;
    private Runnable runnable;
    private Manifest manifest;
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        manifest = Constant.Manifests.get(0);
        TextView txtRoute = (TextView) findViewById(R.id.txt_route_name);
        txtRoute.setText(manifest.getRouteFullName());

        TextView txtChaperoneName = (TextView) findViewById(R.id.txt_chaperone_name);
        txtChaperoneName.setText(manifest.ManifestItems.get(Global.SELECTED_ITEM_INDEX).getAddressDetail());

        adapter = new RecyclerAdapter();
        adapter.setItems(manifest.ManifestItems.get(Global.SELECTED_ITEM_INDEX).ManifestItemDetails);
        recyclerView = (RecyclerView) findViewById(R.id.list_point);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.setAdapter(adapter);
    }

    public void skipManifestItemDetail(ManifestItemDetail item) {
        for (int j = 0; j < Constant.Manifests.get(0).ListStudents.size(); j++) {
            if(Constant.Manifests.get(0).ListStudents.get(j).Id == item.Id) {
                Constant.Manifests.get(0).ListStudents.get(j).isSkip = item.isSkip;
                Constant.Manifests.get(0).ListStudents.get(j).SkipTime = item.SkipTime;
                Constant.Manifests.get(0).ListStudents.get(j).DriverPlannedSkipTime = item.DriverPlannedSkipTime;
            }
        }
    }

    class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnTouchListener {
        private List<ManifestItemDetail> items;

        public RecyclerAdapter()
        {
            //this.items = new ArrayList<>();
        }

        public void setItems(List<ManifestItemDetail> items)
        {
            if(this.items == null)
                this.items = new ArrayList<>();

            this.items.clear();
            this.items.addAll(items);
            notifyDataSetChanged();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(StudentsActivity.this).inflate(R.layout.item_student, parent, false);
            return new RecyclerAdapter.ItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            RecyclerAdapter.ItemViewHolder iv = (RecyclerAdapter.ItemViewHolder) holder;
            final ManifestItemDetail item = items.get(position);
            iv.txtName.setText(item.MemberName);
            iv.txtAddress.setText(item.MemberDetail);
            iv.txtInstruction.setText(item.Instruction);

            iv.btnSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.isSkip = !item.isSkip;
                    item.SkipTime = item.isSkip ? new Date() : null;
                    item.DriverPlannedSkipTime = item.isSkip ? new Date() : null;
                    notifyItemChanged(position);
                    if(item.isSkip)
                        skipManifestItemDetail(item);
                }
            });

            if(item.isSkip)
                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_pressed);
            else
                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_not_pressed);

            if(item.PlannedSkipTime != null)
            {
                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_pressed);
                iv.btnSkip.setEnabled(true);
            }
            else
            {
                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_not_pressed);
                iv.btnSkip.setEnabled(true);
            }

//            final MenuItem item = items.get(position);
//
//            ((ItemViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    showDialog(100);
//                }
//            });
//            Picasso.with(PromoListActivity.this)
//                    .load(item.ImgUrl.replace(" ", "%20"))
////                            //.placeholder((position - HEADER_COUNT) % 2 == 0 ? R.drawable.default_image_large : R.drawable.default_image_small)
////                            //.error(R.drawable.flag_id)
//                    .into(((ItemViewHolder) holder).promo);

            if(position == getItemCount()-1) {
                // If so, add some margin to the element
                iv.setBottomMargin(R.dimen.last_item_offset); // some dimension (or set a fixed amount of pixels
            }
            else
                iv.setBottomMargin(R.dimen.default_item_offset);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        class ItemViewHolder extends RecyclerView.ViewHolder {
            public TextView txtNo;
            public TextView txtName;
            public TextView txtAddress;
            public TextView txtInstruction;
            public Button btnSkip;
            private View mRootView;

            public ItemViewHolder(View itemView) {
                super(itemView);
                mRootView = itemView;
                txtNo  = (TextView) itemView.findViewById(R.id.item_no);
                txtName  = (TextView) itemView.findViewById(R.id.item_name);
                txtAddress  = (TextView) itemView.findViewById(R.id.item_address);
                txtInstruction  = (TextView) itemView.findViewById(R.id.item_instruction);
                btnSkip  = (Button) itemView.findViewById(R.id.btn_skip);
            }

            public void setBottomMargin(@DimenRes int margin){
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) mRootView.getLayoutParams();
                layoutParams.bottomMargin = (int)getResources().getDimension(margin);
                mRootView.setLayoutParams(layoutParams);
            }

        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }
}
