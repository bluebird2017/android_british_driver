package com.armanovus.britishdriver.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.armanovus.britishdriver.Constant;
import com.armanovus.britishdriver.Global;
import com.armanovus.britishdriver.R;
import com.armanovus.britishdriver.listener.ResponseListener;
import com.armanovus.britishdriver.models.ApiParam;
import com.armanovus.britishdriver.models.Manifest;
import com.armanovus.britishdriver.models.ManifestItem;
import com.armanovus.britishdriver.models.ManifestItemDetail;
import com.armanovus.britishdriver.models.NoteType;
import com.armanovus.britishdriver.models.param.CheckinBaseParam;
import com.armanovus.britishdriver.models.param.DriverBaseParam;
import com.armanovus.britishdriver.models.response.LoginResponse;
import com.armanovus.britishdriver.models.response.ManifestResponse;
import com.armanovus.britishdriver.models.response.ResultBase;
import com.armanovus.britishdriver.util.Utility;
import com.armanovus.britishdriver.util.VolleyHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A login screen
 */
public class LoginActivity extends AppCompatActivity implements ResponseListener {
    private final int REQUEST_LOGIN = 1;
    private final int REQUEST_MANIFEST = 2;
    private final int REQUEST_ACK = 3;
    private final int REQUEST_MANIFEST_OPEN = 4;
    private final int REQUEST_NOTE_TYPES = 5;
    private final int REQUEST_CHECK_VERSION = 6;
    private EditText txtUsername;
    private EditText txtPassword;
    private Button btnLogin;
    private View mProgressView;
    private View mLoginFormView;
    private String deviceId;
    private GradientDrawable drawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUsername = (EditText) findViewById(R.id.txt_username);
        txtPassword = (EditText) findViewById(R.id.txt_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        checkLocationPermission();

        mProgressView = findViewById(R.id.login_progress);
        mLoginFormView = findViewById(R.id.login_form);

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        String username = sharedPref.getString(getString(R.string.user_data), "");

        if(!TextUtils.isEmpty(username))
            txtUsername.setText(username);

        //txtUsername.setText("00135026");
        //txtUsername.setText("00099844");
        //txtUsername.setText("00024251");
        //txtUsername.setText("00135026");// CUCUP HENDRAWAN
        //txtUsername.setText("00161599");// EKWAN HARYANA
        txtPassword.setText("123456");

        drawable = (GradientDrawable) btnLogin.getBackground();
        drawable.setColor(Color.parseColor("#4abb47"));

        //deviceId = "0100D8F70200D0B103000249040035D905000BCA0700B3BA08002A4E0900FBF1";
        //DriverBaseParam.HARDWARE_ID = "0100D8F70200D0B103000249040035D905000BCA0700B3BA08002A4E0900FBF1";
        deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        DriverBaseParam.HARDWARE_ID = deviceId;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        checkVersion();
        Global.loadNotes();
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        txtUsername.setError(null);
        txtPassword.setError(null);
        drawable.setColor(Color.parseColor("#664abb47"));

        // Store values at the time of the login attempt.
        String username = txtUsername.getText().toString();
        String password = txtPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            txtPassword.setError(getString(R.string.error_invalid_password));
            focusView = txtPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(username)) {
            txtUsername.setError(getString(R.string.error_field_required));
            focusView = txtUsername;
            cancel = true;
        }

        if (cancel) {
            drawable.setColor(Color.parseColor("#4abb47"));
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
//            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, R.style.CustomDialog);
//            // Add the buttons
//            builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//                }
//            });
//            builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//
//                }
//            });
//
//            builder.setMessage("Anda yakin telah sampai di tujuan penjemputan?")
//                    .setTitle("Konfirmasi");
//
//            android.app.AlertDialog dialog = builder.create();
//            dialog.show();

            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            showProgress(true);
            login();
            loadNoteTypes();
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() == 6;
    }

    private void login() {
//        ProgressDialog _progressDialog = new ProgressDialog(this);
//        _progressDialog.setIndeterminate(true);
//        _progressDialog.setCancelable(false);
//        _progressDialog.setMessage("Loading...");
//        _progressDialog.show();

        String url = Constant.BASE_URL + Constant.LOGIN_API;
        JSONObject objParam = new JSONObject();

        String username = txtUsername.getText().toString();
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.user_data), username);
        editor.commit();
        username = Utility.padLeftZeros(username, 8);

        try {
            objParam.put("un", username);
            objParam.put("pin", txtPassword.getText().toString());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ"); // This should work for you. Though I must say 6 "S" is not done. You won't get milliseconds for 6 precisions.
            String dateString = dateFormat.format(Utility.getDate());
            objParam.put("pdt", dateString);
            objParam.put("did", deviceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String params = objParam.toString();
        VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_LOGIN, true);
    }

    private void loadData(LoginResponse loginResponse) {
        String url = Constant.BASE_URL + Constant.DOWNLOAD_MANIFEST_API;
        JSONObject objParam = new JSONObject();
        try {
            objParam.put("un", loginResponse.DriverCode);
            objParam.put("tid", 0);
            objParam.put("pc", loginResponse.PermitCode);
            objParam.put("py", loginResponse.PermitYear);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ"); // This should work for you. Though I must say 6 "S" is not done. You won't get milliseconds for 6 precisions.
            String dateString = dateFormat.format(Utility.getDate());
            objParam.put("pdt", dateString);
            objParam.put("od", dateString);
            objParam.put("did", deviceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String params = objParam.toString();
        VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_MANIFEST, true);
    }

    private void manifetstAck(Manifest manifest) {
        String url = Constant.BASE_URL + Constant.MANIFEST_ACK_API;
        JSONObject objParam = new JSONObject();
        try {
            objParam.put("mid", manifest.Id);
            objParam.put("adid", Global.DriverData.ActiveDriverId);
            objParam.put("un", Global.DriverData.DriverCode);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ"); // This should work for you. Though I must say 6 "S" is not done. You won't get milliseconds for 6 precisions.
            String dateString = dateFormat.format(Utility.getDate());
            objParam.put("pdt", dateString);
            objParam.put("did", deviceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String params = objParam.toString();
        VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_ACK, true);
    }

    private void loadNoteTypes() {
        String url = Constant.BASE_URL + Constant.NOTE_TYPES_API;

        VolleyHelper.getInstance(this).getResponse(url, this, REQUEST_NOTE_TYPES, false);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if(!show)
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

//            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
//                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//                }
//            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            //mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission. ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this, R.style.CustomDialog)
                        .setTitle("location permission")
                        .setMessage("Allow device to use location?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(LoginActivity.this,
                                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission. ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    public void manifestOpen()
    {
        String url = Constant.BASE_URL + Constant.MANIFEST_OPEN_API;
        Location loc = Global.GetLocation(this);

        CheckinBaseParam objParam = new CheckinBaseParam();
        objParam.ActiveDriverId = Global.DriverData.ActiveDriverId;
        objParam.Username = Global.DriverData.Username;
        objParam.Latitude = 0;
        objParam.Longitude = 0;
        if(loc != null)
        {
            objParam.Latitude = loc.getLatitude();
            objParam.Longitude = loc.getLongitude();
        }
        objParam.ManifestId = Constant.Manifests.get(0).Id;
        //objParam.TrackingStatus = 2;
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ").create();
        final String params = gson.toJson(objParam);
        //VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_MANIFEST_OPEN, false);
        ApiParam apiParam = new ApiParam();
        apiParam.Url = url;
        apiParam.Params = params;
        VolleyHelper.getInstance(this).addApiParam(apiParam);
    }

    public void checkVersion()
    {
        showProgress(true);
        String url = Constant.BASE_URL + Constant.CHECK_VERSION_API;
        VolleyHelper.getInstance(this).getResponse(url, this, REQUEST_CHECK_VERSION, true);
        //var result = await _api.ManifestStart(param);
        //var aq = new ApiQueue { Key = ApiType.ManifestStart, ApiParam = param };
        //QueueParam.Enqueue(aq);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Location lastKnownLocation = null;
                    // permission was granted
                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission. ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        String locationProvider = LocationManager.GPS_PROVIDER;
                        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        //locationManager.requestLocationUpdates(provider, 400, 1, this);
                        lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }

    @Override
    public void onResponse(String response, int requestCode) {
        if(requestCode == REQUEST_LOGIN) {
            Gson gson = new Gson();
            LoginResponse loginResponse = gson.fromJson(response, LoginResponse.class);
            if(loginResponse.IsError) {
                Utility.showAlert(this, loginResponse.ErrorMessage);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                showProgress(false);
                drawable.setColor(Color.parseColor("#4aba47"));
            }
            else {
                loginResponse.Username = txtUsername.getText().toString();
                Global.DriverData = loginResponse;
                loadData(loginResponse);
            }
        }
        else if(requestCode == REQUEST_MANIFEST)
        {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
            List<Manifest> manifests = gson.fromJson(response, new TypeToken<List<Manifest>>(){}.getType());

            if(manifests == null || manifests.size() == 0)
            {
                showProgress(false);
                Utility.showAlert(this, "Tidak ada order.");
                drawable.setColor(Color.parseColor("#4aba47"));
                return;
            }

            try {
                Collections.sort(manifests, new Comparator<Manifest>() {
                    @Override
                    public int compare(Manifest o1, Manifest o2) {
                        return o1.ChaperonePickupTime.compareTo(o2.ChaperonePickupTime);
                    }
                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            try {
                Collections.sort(manifests, new Comparator<Manifest>() {
                    @Override
                    public int compare(Manifest o1, Manifest o2) {
                        return o1.ManifestItems.get(0).PickupTime.compareTo(o2.ManifestItems.get(0).PickupTime);
                    }
                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            Manifest manifest = manifests.get(0);
            manifest.ListStudents = new ArrayList<>();
            for (int i = 0; i < manifest.ManifestItems.size(); i++) {
                ManifestItem mi = manifest.ManifestItems.get(i);
                // if AM and gate point
                if (manifest.RouteType == 0 && mi.ManifestItemType == Constant.MANIFEST_ITEM_TYPE_CHECKPOINT)
                    break;

                // if PM, overlook pickup point
                if (manifest.RouteType == 1 && mi.ManifestItemType == Constant.MANIFEST_ITEM_TYPE_PICKUP)
                    continue;

                //if (Manifest.RouteType == RouteType.AM && item.ManifestItemDetails.Count == 0)
                //    break;
                for (int j = 0; j < mi.ManifestItemDetails.size(); j++) {
                    mi.ManifestItemDetails.get(j).ManifestItemId = mi.Id;
                    //mi.ManifestItemDetails.get(j).ManifestItemType = manifest.RouteType == 0 ? Constant.MANIFEST_ITEM_TYPE_DROP : Constant.MANIFEST_ITEM_TYPE_PICKUP;
                    manifest.ListStudents.add(mi.ManifestItemDetails.get(j));
                }
            }

            Collections.sort(manifest.ListStudents, new Comparator<ManifestItemDetail>() {
                @Override
                public int compare(ManifestItemDetail o1, ManifestItemDetail o2) {
                    return o1.MemberName.toUpperCase().compareTo(o2.MemberName.toUpperCase());
                }
            });
            Constant.Manifests = manifests;
            manifetstAck(manifest);
        }
        else if(requestCode == REQUEST_ACK)
        {
            manifestOpen();
            Intent intent = new Intent(LoginActivity.this, StudentListActivity.class);
            startActivity(intent);
            showProgress(false);
            drawable.setColor(Color.parseColor("#4abb47"));
        }
        else if(requestCode == REQUEST_NOTE_TYPES) {
            Gson gson = new Gson();
            try{
                List<NoteType> noteTypes = gson.fromJson(response, new TypeToken<List<NoteType>>(){}.getType());
                if(noteTypes != null)
                    Global.NoteTypes = noteTypes;
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        else if(requestCode == REQUEST_CHECK_VERSION) {
            Gson gson = new Gson();
            ResultBase result = gson.fromJson(response, ResultBase.class);
            String version = getResources().getString(R.string.app_version).substring(1);
            if (!result.Version.ValueData.equals(version))
            {
                String urlString = String.format("%s%s?%s", Constant.BASE_URL, result.Path.ValueData, version);
                Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setPackage("com.android.chrome");
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    // Chrome browser presumably not installed so allow user to choose instead
                    intent.setPackage(null);
                    startActivity(intent);
                }
            }

            showProgress(false);
        }
    }

    @Override
    public void onErrorResponse(int requestCode) {
        drawable.setColor(Color.parseColor("#4aba47"));
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        showProgress(false);
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}

