package com.armanovus.britishdriver.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.armanovus.britishdriver.Constant;
import com.armanovus.britishdriver.Global;
import com.armanovus.britishdriver.R;
import com.armanovus.britishdriver.fragments.PickDropFragment;
import com.armanovus.britishdriver.fragments.PickDropListStudentFragment;
import com.armanovus.britishdriver.listener.ResponseListener;
import com.armanovus.britishdriver.models.ApiParam;
import com.armanovus.britishdriver.models.Manifest;
import com.armanovus.britishdriver.models.ManifestItem;
import com.armanovus.britishdriver.models.ManifestItemDetail;
import com.armanovus.britishdriver.models.Note;
import com.armanovus.britishdriver.models.param.ActionDetailParam;
import com.armanovus.britishdriver.models.param.CheckinBaseParam;
import com.armanovus.britishdriver.models.param.DriverBaseParam;
import com.armanovus.britishdriver.models.param.ManifestItemCheckinParam;
import com.armanovus.britishdriver.models.param.ManifestItemDetailActionParam;
import com.armanovus.britishdriver.util.Utility;
import com.armanovus.britishdriver.util.VolleyHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class OrderActivity extends AppCompatActivity implements ResponseListener {
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private PickDropFragment pickDropFragment;
    private PickDropListStudentFragment pickDropListStudentFragment;
    private TextView txtRoute;
    private LinearLayout layoutHeaderDetail;
    private LinearLayout layoutHeaderCount;
    private TextView txtAbsentCount;
    private TextView txtPresentCount;
    private int selectedTab;
    private Manifest manifest;
    private View mProgressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mProgressView = findViewById(R.id.login_progress);

        layoutHeaderDetail = (LinearLayout) findViewById(R.id.layout_header_detail);
        layoutHeaderCount = (LinearLayout) findViewById(R.id.layout_header_count);
        txtAbsentCount = (TextView) findViewById(R.id.txt_absent_count);
        txtPresentCount = (TextView) findViewById(R.id.txt_present_count);

        manifest = Constant.Manifests.get(0);
        txtRoute = (TextView) findViewById(R.id.txt_route_name);
        txtRoute.setText(manifest.getRouteFullName());

        TextView txtChaperoneName = (TextView) findViewById(R.id.txt_chaperone_name);
        TextView txtChaperonePickup = (TextView) findViewById(R.id.txt_chaperone_pickup);
        txtChaperoneName.setText(manifest.getChaperone());
        txtChaperonePickup.setText(manifest.getChaperonePickup());

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                mViewPager.setCurrentItem(selectedTab);
                if(selectedTab == 1)
                {
                    // pickup chaperone
                    if(Global.CURRENT_POINT_INDEX == 0) {
                        tab.setText("CHAPERONE");
                        txtRoute.setText(manifest.getRouteFullName());
                        layoutHeaderDetail.setVisibility(View.VISIBLE);
                        layoutHeaderCount.setVisibility(View.GONE);
                        menu.findItem(R.id.action_check_all).setVisible(false);
                    }
                    else // pickup student
                    {
                        tab.setText("DAFTAR SISWA");
                        txtRoute.setText("TOTAL SISWA: " + manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX -1).ManifestItemDetails.size());
                        layoutHeaderDetail.setVisibility(View.GONE);
                        layoutHeaderCount.setVisibility(View.VISIBLE);
                        if(manifest.IsCheckin)
                            menu.findItem(R.id.action_check_all).setVisible(true);
                    }
                    pickDropFragment.loadData();
                    pickDropListStudentFragment.loadData(manifest);
                    updateCount();
                }
                else
                {
                    menu.findItem(R.id.action_check_all).setVisible(false);
                    txtRoute.setText(manifest.getRouteFullName());
                    layoutHeaderDetail.setVisibility(View.VISIBLE);
                    layoutHeaderCount.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        pickDropFragment = new PickDropFragment();
        pickDropListStudentFragment = new PickDropListStudentFragment();
    }

    public void skipManifestItemDetail(ManifestItemDetail item) {
        for (int j = 0; j < manifest.ListStudents.size(); j++) {
            if(manifest.ListStudents.get(j).Id == item.Id) {
                manifest.ListStudents.get(j).isSkip = item.isSkip;
                manifest.ListStudents.get(j).SkipTime = item.SkipTime;
                manifest.ListStudents.get(j).DriverPlannedSkipTime = item.DriverPlannedSkipTime;
                manifest.ListStudents.get(j).PickDropTime = item.PickDropTime;
            }
        }
    }

    public void updateCount(){
        updateAbsentCount();
        updatePresentCount();
    }

    public void updateAbsentCount()
    {
        if(Global.CURRENT_POINT_INDEX > manifest.ManifestItems.size() || Global.CURRENT_POINT_INDEX == 0) return;
        if(manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1).ManifestItemType == Constant.MANIFEST_ITEM_TYPE_PICKUP)
            txtAbsentCount.setText(Integer.toString(manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1).getAbsentCount()));
        else
        {
            // if on gate 2
//            int count = 0;
//            for (int i = 0; i < manifest.ListStudents.size(); i++) {
//                if(manifest.ListStudents.get(i).SkipTime != null || manifest.ListStudents.get(i).PlannedSkipTime != null || manifest.ListStudents.get(i).DriverPlannedSkipTime != null)
//                    count++;
//            }
            //txtAbsentCount.setText(Integer.toString(count));
            txtAbsentCount.setText(Integer.toString(manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1).getAbsentCount()));
        }
    }

    public void updatePresentCount()
    {
        if(Global.CURRENT_POINT_INDEX > manifest.ManifestItems.size() || Global.CURRENT_POINT_INDEX == 0) return;
        if(Global.CURRENT_POINT_INDEX > manifest.ManifestItems.size()) return;
        if(manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1).ManifestItemType == Constant.MANIFEST_ITEM_TYPE_PICKUP)
            txtPresentCount.setText(Integer.toString(manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1).getPresentCount()));
        else
        {
            // if on gate 2
//            int count = 0;
//            for (int i = 0; i < manifest.ListStudents.size(); i++) {
//                if(manifest.ListStudents.get(i).PickDropTime != null && manifest.ListStudents.get(i).PlannedSkipTime == null)
//                    count++;
//            }
//            txtPresentCount.setText(Integer.toString(count));
            txtPresentCount.setText(Integer.toString(manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1).getPresentCount()));
        }
    }

    public void goToNextDestination() {
        if(Global.CURRENT_POINT_INDEX == 0) {
            doChaperoneDepart();
            tabLayout.getTabAt(1).setText("DAFTAR SISWA");
        }
        else
        {
            manifestItemDetailAction();
        }
        Global.CURRENT_POINT_INDEX++;

//        else
//            tabLayout.getTabAt(1).setText("CHAPERONE");
        if(Global.CURRENT_POINT_INDEX > manifest.ManifestItems.size())
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(OrderActivity.this, R.style.CustomDialog);
            builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    manifestFinish();
                    showCloseOrderDialog();
                }
            });
            builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });

            builder.setCancelable(false);
            builder.setMessage("Anda yakin akan melakukan finish?")
                    .setTitle("Konfirmasi");

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        else {
            while (manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1).isSkip)
            {
                Global.CURRENT_POINT_INDEX++;
            }
            updateCount();
            pickDropFragment.loadData();
            mViewPager.setCurrentItem(0);
        }
    }


    public void goToPickupList() {
        if(Global.CURRENT_POINT_INDEX == 0)
            checkInChaperone();
        else
            manifestItemCheckin(5);

        if(Global.CURRENT_POINT_INDEX > 0 && manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1).ManifestItemType == 2)
        {
            Global.CURRENT_POINT_INDEX++;
            manifest.IsCheckin = false;
            pickDropFragment.loadData();
            updateCount();
            pickDropListStudentFragment.loadData(manifest);
            mViewPager.setCurrentItem(0);
            return;
        }
        else {
            mViewPager.setCurrentItem(1);
        }
        pickDropListStudentFragment.loadData(manifest);

    }

    private Button btnCloseOrder;
    private EditText txtNotes;
    private Dialog dialogCloseOrder;
    public void showCloseOrderDialog()
    {
        dialogCloseOrder = new Dialog(this);
        dialogCloseOrder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCloseOrder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogCloseOrder.setContentView(R.layout.dialog_close_order);
        dialogCloseOrder.setCancelable(false);

        txtNotes = (EditText) dialogCloseOrder.findViewById(R.id.txt_close_notes);
        btnCloseOrder = (Button) dialogCloseOrder.findViewById(R.id.btn_close_order);
        btnCloseOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hideSoftKeyboard();
                dialogCloseOrder.dismiss();
                if(!TextUtils.isEmpty(txtNotes.getText()))
                    sendCloseNotes(txtNotes.getText().toString());
                manifestClose();
            }
        });

        dialogCloseOrder.show();
    }

    public static int REQUEST_CHECKIN = 1;
    public static int REQUEST_CHAPERONE_ONBOARD = 2;
    public static int REQUEST_MANIFEST_ITEM_CHECKIN = 3;
    public static int REQUEST_MANIFEST_ITEM_DETAIL_ACTION_API = 4;
    public static int REQUEST_MANIFEST_START = 5;
    public static int REQUEST_MANIFEST_FINISH = 6;
    public static int REQUEST_MANIFEST_CLOSE = 7;
    public static int REQUEST_CREATE_NOTE = 8;
    private void checkInChaperone() {
        String url = Constant.BASE_URL + Constant.CHAPERONE_CHECKIN_API;

        Location loc = Global.GetLocation(this);
        CheckinBaseParam objParam = new CheckinBaseParam();
        objParam.ActiveDriverId = Global.DriverData.ActiveDriverId;
        objParam.Username = Global.DriverData.Username;
        objParam.Latitude = 0;
        objParam.Longitude = 0;
        if(loc != null)
        {
            objParam.Latitude = loc.getLatitude();
            objParam.Longitude = loc.getLongitude();
        }
        objParam.ManifestId = manifest.Id;
        objParam.TrackingStatus = 3;

        Gson gson = new Gson();
        final String params = gson.toJson(objParam);
        //VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_CHECKIN, false);
        ApiParam apiParam = new ApiParam();
        apiParam.Url = url;
        apiParam.Params = params;
        VolleyHelper.getInstance(this).addApiParam(apiParam);
    }

    private void doChaperoneDepart() {
        String url = Constant.BASE_URL + Constant.CHAPERONE_ONBOARD_API;

        Location loc = Global.GetLocation(this);
        CheckinBaseParam objParam = new CheckinBaseParam();
        objParam.ActiveDriverId = Global.DriverData.ActiveDriverId;
        objParam.Username = Global.DriverData.Username;
        objParam.Latitude = 0;
        objParam.Longitude = 0;
        if(loc != null)
        {
            objParam.Latitude = loc.getLatitude();
            objParam.Longitude = loc.getLongitude();
        }
        objParam.ManifestId = manifest.Id;
        objParam.TrackingStatus = 4;

        Gson gson = new Gson();
        final String params = gson.toJson(objParam);
        if(manifest.ChaperoneSkipTime != null)
            url = Constant.BASE_URL + Constant.CHAPERONE_SKIP_API;
        //VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_CHAPERONE_ONBOARD, false);
        ApiParam apiParam = new ApiParam();
        apiParam.Url = url;
        apiParam.Params = params;
        VolleyHelper.getInstance(this).addApiParam(apiParam);
    }

    public void manifestItemCheckin(int status)
    {
        String url = Constant.BASE_URL + Constant.MANIFEST_ITEM_CHECKIN_API;
        Location loc = Global.GetLocation(this);

        ManifestItemCheckinParam objParam = new ManifestItemCheckinParam();
        objParam.ActiveDriverId = Global.DriverData.ActiveDriverId;
        objParam.Username = Global.DriverData.Username;
        objParam.Latitude = 0;
        objParam.Longitude = 0;
        if(loc != null)
        {
            objParam.Latitude = loc.getLatitude();
            objParam.Longitude = loc.getLongitude();
        }
        objParam.ManifestId = manifest.Id;
        objParam.ManifestItemId = manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1).Id;
        objParam.TrackingStatus = status;

        Gson gson = new Gson();
        final String params = gson.toJson(objParam);
        //VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_MANIFEST_ITEM_CHECKIN, false);
        ApiParam apiParam = new ApiParam();
        apiParam.Url = url;
        apiParam.Params = params;
        VolleyHelper.getInstance(this).addApiParam(apiParam);
    }

    public void manifestItemDetailAction()
    {
        String url = Constant.BASE_URL + Constant.MANIFEST_ITEM_DETAIL_ACTION_CHECKIN_API;
        Location loc = Global.GetLocation(this);

        ManifestItemDetailActionParam objParam = new ManifestItemDetailActionParam();
        objParam.ActiveDriverId = Global.DriverData.ActiveDriverId;
        objParam.Username = Global.DriverData.Username;
        objParam.Latitude = 0;
        objParam.Longitude = 0;
        if(loc != null)
        {
            objParam.Latitude = loc.getLatitude();
            objParam.Longitude = loc.getLongitude();
        }
        objParam.ManifestId = manifest.Id;
        ManifestItem mi = manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1);
        objParam.ManifestItemId = mi.Id;
        objParam.TrackingStatus = 6;
        objParam.ActionDetails = new ArrayList<>();
        List<ManifestItemDetail> mids = mi.ManifestItemDetails;
        for (int i = 0; i < mids.size(); i++) {
            ActionDetailParam action = new ActionDetailParam();
            action.ActionTime = mids.get(i).PickDropTime;
            action.SkipTime = mids.get(i).SkipTime;
            action.ActionType = mi.ManifestItemType;
            action.DetailId = mids.get(i).Id;
            objParam.ActionDetails.add(action);
        }

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ").create();
        final String params = gson.toJson(objParam);
        //VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_MANIFEST_ITEM_DETAIL_ACTION_API, false);
        ApiParam apiParam = new ApiParam();
        apiParam.Url = url;
        apiParam.Params = params;
        VolleyHelper.getInstance(this).addApiParam(apiParam);
    }

    public void manifestStart()
    {
        String url = Constant.BASE_URL + Constant.MANIFEST_START_API;
        Location loc = Global.GetLocation(this);

        CheckinBaseParam objParam = new CheckinBaseParam();
        objParam.ActiveDriverId = Global.DriverData.ActiveDriverId;
        objParam.Username = Global.DriverData.Username;
        objParam.Latitude = 0;
        objParam.Longitude = 0;
        if(loc != null)
        {
            objParam.Latitude = loc.getLatitude();
            objParam.Longitude = loc.getLongitude();
        }
        objParam.ManifestId = manifest.Id;
        objParam.TrackingStatus = 2;
        final String params = objParam.toString();
        //VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_MANIFEST_START, false);
        ApiParam apiParam = new ApiParam();
        apiParam.Url = url;
        apiParam.Params = params;
        VolleyHelper.getInstance(this).addApiParam(apiParam);
    }

    public void manifestFinish()
    {
        String url = Constant.BASE_URL + Constant.MANIFEST_FINISH_API;
        Location loc = Global.GetLocation(this);

        CheckinBaseParam objParam = new CheckinBaseParam();
        objParam.ActiveDriverId = Global.DriverData.ActiveDriverId;
        objParam.Username = Global.DriverData.Username;
        objParam.Latitude = 0;
        objParam.Longitude = 0;
        if(loc != null)
        {
            objParam.Latitude = loc.getLatitude();
            objParam.Longitude = loc.getLongitude();
        }
        objParam.ManifestId = manifest.Id;
        objParam.TrackingStatus = 8;
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ").create();
        final String params = gson.toJson(objParam);
        //VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_MANIFEST_FINISH, false);
        ApiParam apiParam = new ApiParam();
        apiParam.Url = url;
        apiParam.Params = params;
        VolleyHelper.getInstance(this).addApiParam(apiParam);
    }

    public void sendCloseNotes(String notes)
    {
        String url = Constant.BASE_URL + Constant.CREATE_NOTE_API;
        Note note = new Note();
        note.NoteTypeId = 9;
        note.ManifestId = manifest.Id;
        note.Details = notes;
        note.Contributor = Global.DriverData.Username;

        Gson gson = new Gson();
        final String params = gson.toJson(note);
        //VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_CREATE_NOTE, false);
        ApiParam apiParam = new ApiParam();
        apiParam.Url = url;
        apiParam.Params = params;
        VolleyHelper.getInstance(this).addApiParam(apiParam);
    }

    public void sendNotes(String notes, int noteType)
    {
        String url = Constant.BASE_URL + Constant.CREATE_NOTE_API;
        Note note = new Note();
        note.NoteTypeId = noteType+2;
        note.ManifestId = manifest.Id;
        note.Details = notes;
        note.Contributor = Global.DriverData.Username;

        Gson gson = new Gson();
        final String params = gson.toJson(note);
        //VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_CREATE_NOTE, false);
        ApiParam apiParam = new ApiParam();
        apiParam.Url = url;
        apiParam.Params = params;
        VolleyHelper.getInstance(this).addApiParam(apiParam);
    }

    public void manifestClose()
    {
        String url = Constant.BASE_URL + Constant.MANIFEST_CLOSE_API;
        Location loc = Global.GetLocation(this);

        CheckinBaseParam objParam = new CheckinBaseParam();
        objParam.ActiveDriverId = Global.DriverData.ActiveDriverId;
        objParam.Username = Global.DriverData.Username;
        objParam.Latitude = 0;
        objParam.Longitude = 0;
        if(loc != null)
        {
            objParam.Latitude = loc.getLatitude();
            objParam.Longitude = loc.getLongitude();
        }
        objParam.ManifestId = manifest.Id;
        objParam.TrackingStatus = 9;

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ").create();
        final String params = gson.toJson(objParam);
        //VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_MANIFEST_CLOSE, true);
        ApiParam apiParam = new ApiParam();
        apiParam.Url = url;
        apiParam.Params = params;
        VolleyHelper.getInstance(this).addApiParam(apiParam);
        VolleyHelper.getInstance(this).sendAllRequest(this);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResponse(String response, int requestCode) {
        if(requestCode == REQUEST_MANIFEST)
        {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
            List<Manifest> manifests = gson.fromJson(response, new TypeToken<List<Manifest>>(){}.getType());

            if(manifests == null || manifests.size() == 0)
            {
                //showProgress(false);
                Utility.showAlert(this, "Tidak ada order.");
                return;
            }

            Manifest manifest = manifests.get(0);
            manifest.ListStudents = new ArrayList<>();
            for (int i = 0; i < manifest.ManifestItems.size(); i++) {
                ManifestItem mi = manifest.ManifestItems.get(i);
                if (manifest.RouteType == 0 && mi.ManifestItemType == Constant.MANIFEST_ITEM_TYPE_CHECKPOINT)
                    break;

                if (manifest.RouteType == 1 && mi.ManifestItemType == Constant.MANIFEST_ITEM_TYPE_DROP)
                    break;

                //if (Manifest.RouteType == RouteType.AM && item.ManifestItemDetails.Count == 0)
                //    break;
                for (int j = 0; j < mi.ManifestItemDetails.size(); j++) {
                    mi.ManifestItemDetails.get(j).ManifestItemId = mi.Id;
                    manifest.ListStudents.add(mi.ManifestItemDetails.get(j));
                }
            }

            Collections.sort(manifest.ListStudents, new Comparator<ManifestItemDetail>() {
                @Override
                public int compare(ManifestItemDetail o1, ManifestItemDetail o2) {
                    return o1.MemberName.toUpperCase().compareTo(o2.MemberName.toUpperCase());
                }
            });
            Constant.Manifests = manifests;
            manifests.get(0).IsCheckin = this.manifest.IsCheckin;
            this.manifest = manifests.get(0);
            pickDropListStudentFragment.loadData(this.manifest);
            pickDropListStudentFragment.refresh();
            pickDropFragment.setManifest(this.manifest);
            updateCount();
            mProgressView.setVisibility(View.GONE);
        }
        else if(requestCode == REQUEST_CHECKIN)
        {

        }
        else if(requestCode == REQUEST_CHAPERONE_ONBOARD)
        {

        }
        else if(requestCode == REQUEST_MANIFEST_START)
        {

        }
        else if(requestCode == REQUEST_MANIFEST_FINISH)
        {

        }
        else if(requestCode == REQUEST_MANIFEST_CLOSE)
        {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            mProgressView.setVisibility(View.GONE);
            AlertDialog.Builder builder = new AlertDialog.Builder(OrderActivity.this, R.style.CustomDialog);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    Intent intent = new Intent(OrderActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });

            builder.setCancelable(false);
            builder.setMessage("Order anda telah selesai.")
                    .setTitle("Pesan");

            AlertDialog dialog = builder.create();
            if(!isFinishing())
                dialog.show();
        }
    }

    @Override
    public void onErrorResponse(int requestCode) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            if(position == 0) {
                //newOrderFragment = new NewOrderFragment();
                return pickDropFragment;
            }
            else if(position == 1){
                //activeOrderFragment = new ActiveOrderFragment();
                return pickDropListStudentFragment;
            }
            else
                return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "TUJUAN";
                case 1:
                    // pickup chaperone
                    if(Global.CURRENT_POINT_INDEX == 0) {
                        return "CHAPERONE";
                    }
                    else // pickup student
                    {
                        return "DAFTAR SISWA";
                    }
            }
            return null;
        }
    }

    private Menu menu;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_attendees, menu);
        menu.findItem(R.id.action_check_all).setVisible(false);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            reloadData();
        }
        else if (id == R.id.action_check_all) {
            ManifestItem mi = manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1);
            if(mi.ManifestItemType == Constant.MANIFEST_ITEM_TYPE_DROP)
            {
//                for (int i = 0; i < manifest.ListStudents.size(); i++) {
//                    ManifestItemDetail mid = manifest.ListStudents.get(i);
//                    if(!mid.isSkip && mid.DriverPlannedSkipTime == null && mid.PlannedSkipTime == null) {
//                        mid.PickDropTime = new Date();
//                        mid.isSkip = false;
//                        mid.SkipTime = null;
//                    }
//                }

                for (int i = 0; i < mi.ManifestItemDetails.size(); i++) {
                    ManifestItemDetail mid = mi.ManifestItemDetails.get(i);
                    if(mid.isOnboard) {
                        mid.PickDropTime = new Date();
                        mid.isSkip = false;
                        mid.SkipTime = null;
                    }
                }
            }
            else {
                for (int i = 0; i < mi.ManifestItemDetails.size(); i++) {
                    ManifestItemDetail mid = mi.ManifestItemDetails.get(i);
                    if(mid.DriverPlannedSkipTime == null && mid.PlannedSkipTime == null) {
                        mid.PickDropTime = new Date();
                        mid.isSkip = false;
                        mid.SkipTime = null;
                    }
                }
            }
            updateCount();
            pickDropListStudentFragment.refresh();
        }

        return super.onOptionsItemSelected(item);
    }

    private final int REQUEST_MANIFEST = 1;
    private void reloadData() {
        mProgressView.setVisibility(View.VISIBLE);;
        String url = Constant.BASE_URL + Constant.DOWNLOAD_MANIFEST_API;
        JSONObject objParam = new JSONObject();
        try {
            objParam.put("un", Global.DriverData.DriverCode);
            objParam.put("tid", 0);
            objParam.put("pc", Global.DriverData.PermitCode);
            objParam.put("py", Global.DriverData.PermitYear);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ"); // This should work for you. Though I must say 6 "S" is not done. You won't get milliseconds for 6 precisions.
            String dateString = dateFormat.format(Utility.getDate());
            objParam.put("pdt", dateString);
            objParam.put("od", dateString);
            objParam.put("did", DriverBaseParam.HARDWARE_ID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String params = objParam.toString();
        VolleyHelper.getInstance(this).getResponse(url, params, this, REQUEST_MANIFEST, true);
    }

    @Override
    public void onBackPressed() {
    }
}
