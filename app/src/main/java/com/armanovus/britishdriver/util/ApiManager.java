package com.armanovus.britishdriver.util;

public class ApiManager {
    private static ApiManager ourInstance = new ApiManager();
    public static ApiManager getInstance() {
        return ourInstance;
    }
    private ApiManager() {
    }
}
