package com.armanovus.britishdriver.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.armanovus.britishdriver.R;

import java.util.Date;


public class Utility {
    public static void showAlert(Context act, String text){
//        AlertDialog.Builder alert = new AlertDialog.Builder(act, R.style.CustomDialog);
//        alert.setMessage(text);
//        alert.setNeutralButton(act.getResources().getString(R.string.ok_button), new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.cancel();
//            }
//        });
//        alert.show();

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(act, R.style.CustomDialog);
            // Add the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            builder.setCancelable(false);
            builder.setMessage(text)
                    .setTitle("Pesan");
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        catch (Exception ex)
        {
            ;
        }
    }

    public static Date getDate()
    {
        Date date = new Date();
//        date.setMonth(3);
//        date.setDate(26);
        return date;
    }

    public static String padLeftZeros(String str, int n) {
        return String.format("%1$" + n + "s", str).replace(' ', '0');
    }
}
