package com.armanovus.britishdriver.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.armanovus.britishdriver.listener.ResponseListener;
import com.armanovus.britishdriver.models.ApiParam;
import com.armanovus.britishdriver.models.response.BaseResponse;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VolleyHelper implements ResponseListener {
    private static VolleyHelper mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static Context mCtx;
    private List<ApiParam> apiParams;

    private VolleyHelper(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
        apiParams = new ArrayList<>();
        gson = new Gson();

        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    public static synchronized VolleyHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new VolleyHelper(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    public void getResponse(String url, final String params, final ResponseListener responseListener, final int requestCode, final boolean showError)
    {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        responseListener.onResponse(response, requestCode);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(showError)
                            Utility.showAlert(mCtx, "Server tidak dapat diakses. Mohon coba beberapa saat lagi.");
                        // TODO: Handle error
                        responseListener.onErrorResponse(requestCode);
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/json");
                headers.put("SoBusy-Auth", "sobusy4356hahahihi");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return params == null ? null : params.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", params, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(stringRequest);
    }

    public void getResponse(String url, final ResponseListener responseListener, final int requestCode, final boolean showError)
    {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        responseListener.onResponse(response, requestCode);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(showError)
                            Utility.showAlert(mCtx, "Server tidak dapat diakses. Mohon coba beberapa saat lagi.");
                        _isSending = false;
                        if(_isSendAllRequest) {
                            sendAllRequest();
                        }
                        else
                        // TODO: Handle error
                            responseListener.onErrorResponse(requestCode);
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/json");
                headers.put("SoBusy-Auth", "sobusy4356hahahihi");
                return headers;
            }
        };

        addToRequestQueue(stringRequest);
    }

    private void sendAllRequest() {
        sendAllRequest(this.listener);
    }

    public void addApiParam(ApiParam param)
    {
        apiParams.add(apiParams.size(), param);
    }

    public ApiParam getApiParam()
    {
        if(apiParams.size() > 0)
            return apiParams.get(0);

        return null;
    }

    public void removeApiParam()
    {
        apiParams.remove(0);
    }

    private boolean _isSending = false;
    private boolean _send = true;
    public void sendRequest()
    {
        _send = true;
        if(apiParams.size() == 0) return;
        if(_isSending) return;

        _isSending = true;
        ApiParam apiParam = apiParams.get(0);
        getResponse(apiParam.Url, apiParam.Params, this, 0, false);
    }

    private boolean _isSendAllRequest;
    private ResponseListener listener;
    public void sendAllRequest(ResponseListener listener)
    {
        _send = true;
        this.listener = listener;
        _isSendAllRequest = true;
        if(apiParams.size() == 0) {
            if(_isSendAllRequest) {
                listener.onResponse("", 7);
            }
            _isSendAllRequest = false;
        }
//        if(_isSending) return;
//
//        _isSending = true;
        ApiParam apiParam = apiParams.get(0);
        getResponse(apiParam.Url, apiParam.Params, this, 0, false);
    }

    private Gson gson;
    @Override
    public void onResponse(String response, int requestCode) {
        BaseResponse result = gson.fromJson(response, BaseResponse.class);
        if (result != null && !result.IsError)
        {
            if(apiParams.size() > 0) {
                apiParams.remove(0);
                if (apiParams.size() == 0 && this.listener != null) {
                    if(_isSendAllRequest && this.listener != null) {
                        this.listener.onResponse("", 7);
                        this.listener = null;
                    }
                    _isSendAllRequest = false;
                }
                else
                    sendRequest();
            }
        }
        else
            _send = false;

        _isSending = false;
    }

    @Override
    public void onErrorResponse(int requestCode) {
        if(_isSendAllRequest) {
            _isSending = false;
            sendAllRequest(this.listener);
        }
    }
}