package com.armanovus.britishdriver.listener;

public interface ResponseListener
{
    void onResponse(String response, int requestCode);
    void onErrorResponse(int requestCode);
}