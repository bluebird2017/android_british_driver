package com.armanovus.britishdriver;

import com.armanovus.britishdriver.models.Manifest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Constant {
//    public static final String BASE_URL = "http://192.168.0.115/sobusy/"; // localhost
//    public static final String BASE_URL = "http://202.47.68.163:8173/sobusydev/"; // development public
    public static final String BASE_URL = "https://operation.bluebirdgroup.com:8443/sobusy/"; // production
    public static final String MANIFEST_GETALL_API = "api/Manifests/GetAll";
    public static final String LOGIN_API = "api/Drivers/DriverLogin";
    public static final String DOWNLOAD_MANIFEST_API = "api/Drivers/GetManifests";
    public static final String MANIFEST_ACK_API = "api/Drivers/ManifestAck";
    //public static String DATE_STATUS_API = "api/Driver/UpdateStatus";
    //public static String ATTENDANCE_API = "api/Driver/Attendances";
    public static final String CHAPERONE_CHECKIN_API = "api/Drivers/ChaperoneCheckin";
    public static final String CHAPERONE_ONBOARD_API = "api/Drivers/ChaperoneOnboard";
    public static final String CHAPERONE_SKIP_API = "api/Drivers/ChaperoneSkip";
    public static final String MANIFEST_ITEM_CHECKIN_API = "api/Drivers/ManifestItemCheckin";
    public static final String MANIFEST_ITEM_DETAIL_ACTION_CHECKIN_API = "api/Drivers/ManifestItemDetailAction";
    public static final String MANIFEST_OPEN_API = "api/Drivers/ManifestOpen";
    public static final String MANIFEST_START_API = "api/Drivers/ManifestStart";
    public static final String CREATE_NOTE_API = "api/Notes/CreateNote";
    public static final String MANIFEST_FINISH_API = "api/Drivers/ManifestFinish";
    public static final String MANIFEST_CLOSE_API = "api/Drivers/ManifestClose";
    public static final String NOTE_TYPES_API = "api/Notes/GetNoteTypes";
    public static final String CHECK_VERSION_API = "api/Drivers/GetAppVersion";
    public static final String PING_API = "api/Drivers/Ping";

    public static final String ROUTE_TYPE_AM = "AM";
    public static final String ROUTE_TYPE_PM = "PM";

    public static final int MANIFEST_ITEM_TYPE_PICKUP = 0;
    public static final int MANIFEST_ITEM_TYPE_DROP = 1;
    public static final int MANIFEST_ITEM_TYPE_CHECKPOINT = 2;
    public static final int MANIFEST_ITEM_TYPE_SPLIT = 3;

    public static List<Manifest> Manifests;

    public static String getRouteType(int id)
    {
        if(id == 0)
            return ROUTE_TYPE_AM;
        else
            return ROUTE_TYPE_PM;
    }

    public static String getHourAndTime(Date time)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        return sdf.format(time);
    }
}
