package com.armanovus.britishdriver.fragments;

import android.os.Bundle;
import android.support.annotation.DimenRes;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.armanovus.britishdriver.Constant;
import com.armanovus.britishdriver.R;
import com.armanovus.britishdriver.models.Manifest;
import com.armanovus.britishdriver.models.ManifestItem;
import com.armanovus.britishdriver.models.ManifestItemDetail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ListStudentFragment extends android.support.v4.app.Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListStudentFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ListStudentFragment newInstance(int columnCount) {
        ListStudentFragment fragment = new ListStudentFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_student_list, container, false);

        Manifest manifest = Constant.Manifests.get(0);
        adapter = new RecyclerAdapter();
        adapter.setItems(manifest.ListStudents);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_point);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recyclerView.setAdapter(adapter);

        return view;
    }

    public void refresh(Manifest manifest) {
        if(adapter != null)
            adapter.setItems(manifest.ListStudents);
    }

    public void refresh() {
        if(adapter != null)
            adapter.notifyDataSetChanged();
    }

    class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnTouchListener {
        private List<ManifestItemDetail> items;

        public RecyclerAdapter()
        {
            //this.items = new ArrayList<>();
        }

        public void setItems(List<ManifestItemDetail> items)
        {
            if(this.items == null)
                this.items = new ArrayList<>();

            this.items.clear();
            this.items.addAll(items);
            notifyDataSetChanged();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_student, parent, false);
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            ItemViewHolder iv = (ItemViewHolder) holder;
            final ManifestItemDetail item = items.get(position);
            iv.txtName.setText(item.MemberName);
            iv.txtAddress.setText(item.MemberDetail);
            iv.txtInstruction.setText(item.Instruction);

            iv.btnSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.isSkip = !item.isSkip;
                    item.SkipTime = item.isSkip ? new Date() : null;
                    item.PickDropTime = item.isSkip ? null : new Date();
                    item.DriverPlannedSkipTime = item.isSkip ? new Date() : null;
                    notifyItemChanged(position);
                }
            });

            if(item.isSkip)
                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_pressed);
            else
                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_not_pressed);


            if(item.PlannedSkipTime != null)
            {
                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_pressed);
                iv.btnSkip.setEnabled(true);
            }
            else
            {
                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_not_pressed);
                iv.btnSkip.setEnabled(true);
            }

//            final MenuItem item = items.get(position);
//
//            ((ItemViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    showDialog(100);
//                }
//            });
//            Picasso.with(PromoListActivity.this)
//                    .load(item.ImgUrl.replace(" ", "%20"))
////                            //.placeholder((position - HEADER_COUNT) % 2 == 0 ? R.drawable.default_image_large : R.drawable.default_image_small)
////                            //.error(R.drawable.flag_id)
//                    .into(((ItemViewHolder) holder).promo);

            if(position == getItemCount()-1) {
                // If so, add some margin to the element
                iv.setBottomMargin(R.dimen.last_item_offset); // some dimension (or set a fixed amount of pixels
            }
            else
                iv.setBottomMargin(R.dimen.default_item_offset);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        class ItemViewHolder extends RecyclerView.ViewHolder {
            public TextView txtNo;
            public TextView txtName;
            public TextView txtAddress;
            public TextView txtInstruction;
            public Button btnSkip;
            private View mRootView;

            public ItemViewHolder(View itemView) {
                super(itemView);
                mRootView = itemView;
                txtNo  = (TextView) itemView.findViewById(R.id.item_no);
                txtName  = (TextView) itemView.findViewById(R.id.item_name);
                txtAddress  = (TextView) itemView.findViewById(R.id.item_address);
                txtInstruction  = (TextView) itemView.findViewById(R.id.item_instruction);
                btnSkip  = (Button) itemView.findViewById(R.id.btn_skip);
            }

            public void setBottomMargin(@DimenRes int margin){
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) mRootView.getLayoutParams();
                layoutParams.bottomMargin = (int)getContext().getResources().getDimension(margin);
                mRootView.setLayoutParams(layoutParams);
            }

        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }


//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnListFragmentInteractionListener) {
//            mListener = (OnListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnListFragmentInteractionListener");
//        }
//    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction();
    }
}
