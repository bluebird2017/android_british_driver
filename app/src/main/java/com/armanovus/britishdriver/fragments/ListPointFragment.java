package com.armanovus.britishdriver.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.DimenRes;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.armanovus.britishdriver.Constant;
import com.armanovus.britishdriver.Global;
import com.armanovus.britishdriver.R;
import com.armanovus.britishdriver.models.Manifest;
import com.armanovus.britishdriver.models.ManifestItem;
import com.armanovus.britishdriver.views.StudentListActivity;
import com.armanovus.britishdriver.views.StudentsActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ListPointFragment extends android.support.v4.app.Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private Manifest manifest;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListPointFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ListPointFragment newInstance(int columnCount) {
        ListPointFragment fragment = new ListPointFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
//        }
    }

    public void refresh(Manifest manifest)
    {
        if(adapter != null)
            adapter.setItems(manifest.ManifestItems);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_point, container, false);

        manifest = Constant.Manifests.get(0);
        adapter = new RecyclerAdapter();
        adapter.setItems(manifest.ManifestItems);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_point);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recyclerView.setAdapter(adapter);

        return view;
    }

    class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnTouchListener {
        private List<ManifestItem> items;

        public RecyclerAdapter()
        {
            //this.items = new ArrayList<>();
        }

        public void setItems(List<ManifestItem> items)
        {
            if(this.items == null)
                this.items = new ArrayList<>();

            this.items.clear();
            this.items.addAll(items);
            notifyDataSetChanged();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_point, parent, false);
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            ItemViewHolder iv = (ItemViewHolder) holder;
            final ManifestItem item = items.get(position);
            iv.txtNo.setText("" + (position + 1));
            iv.txtAddress.setText(item.getAddressDetail());
            iv.txtTime.setText(item.getPickupTime());
            iv.btnSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.isSkip = !item.isSkip;
                    notifyItemChanged(position);
                    ((StudentListActivity)getActivity()).skipManifestItem(item);
                }
            });

            if(item.checkIsGate())
                iv.btnDetail.setVisibility(View.GONE);
            else
            {
                iv.btnDetail.setText(String.format("detil (%s)", item.ManifestItemDetails.size()));
                iv.btnDetail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Global.SELECTED_ITEM_INDEX = position;
                        Intent i = new Intent(getActivity(), StudentsActivity.class);
                        startActivity(i);
                    }
                });
            }

            if(item.isSkip)
                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_pressed);
            else
                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_not_pressed);

            if(manifest.RouteType == 0) {
                if (item.ManifestItemType == 0)
                    iv.btnSkip.setVisibility(View.VISIBLE);
                else
                    iv.btnSkip.setVisibility(View.GONE);
            }
            else
            {
                if (item.ManifestItemType == 1)
                    iv.btnSkip.setVisibility(View.VISIBLE);
                else
                    iv.btnSkip.setVisibility(View.GONE);
            }

            if(item.checkAllSkip())
            {
                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_pressed);
                iv.btnSkip.setEnabled(true);
            }
            else
            {
                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_not_pressed);
                iv.btnSkip.setEnabled(true);
            }

            if(position == getItemCount()-1) {
                // If so, add some margin to the element
                iv.setBottomMargin(R.dimen.last_item_offset); // some dimension (or set a fixed amount of pixels
            }
            else {
                iv.setBottomMargin(R.dimen.default_item_offset);
            }
//            final MenuItem item = items.get(position);
//
//            ((ItemViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    showDialog(100);
//                }
//            });
//            Picasso.with(PromoListActivity.this)
//                    .load(item.ImgUrl.replace(" ", "%20"))
////                            //.placeholder((position - HEADER_COUNT) % 2 == 0 ? R.drawable.default_image_large : R.drawable.default_image_small)
////                            //.error(R.drawable.flag_id)
//                    .into(((ItemViewHolder) holder).promo);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        class ItemViewHolder extends RecyclerView.ViewHolder {
            public TextView txtNo;
            public TextView txtAddress;
            public TextView txtTime;
            public Button btnSkip;
            public Button btnDetail;
            private View mRootView;

            public ItemViewHolder(View itemView) {
                super(itemView);
                mRootView = itemView;
                txtNo  = (TextView) itemView.findViewById(R.id.item_no);
                txtAddress  = (TextView) itemView.findViewById(R.id.item_address);
                txtTime  = (TextView) itemView.findViewById(R.id.item_time);
                btnSkip  = (Button) itemView.findViewById(R.id.btn_skip);
                btnDetail  = (Button) itemView.findViewById(R.id.btn_detail);
            }

            public void setBottomMargin(@DimenRes int margin){
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) mRootView.getLayoutParams();
                layoutParams.bottomMargin = (int)getContext().getResources().getDimension(margin);
                mRootView.setLayoutParams(layoutParams);
            }
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }


//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnListFragmentInteractionListener) {
//            mListener = (OnListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnListFragmentInteractionListener");
//        }
//    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction();
    }
}
