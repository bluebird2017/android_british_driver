package com.armanovus.britishdriver.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.DimenRes;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.armanovus.britishdriver.Constant;
import com.armanovus.britishdriver.Global;
import com.armanovus.britishdriver.R;
import com.armanovus.britishdriver.models.Manifest;
import com.armanovus.britishdriver.models.ManifestItem;
import com.armanovus.britishdriver.models.ManifestItemDetail;
import com.armanovus.britishdriver.util.Utility;
import com.armanovus.britishdriver.views.OrderPMActivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class PickDropListStudentPMFragment extends android.support.v4.app.Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private CardView layoutChaperone;
    private TextView txtChapName;
    private Button btnPresentChap;
    private Button btnAbsentChap;
    private Button btnDepart;
    private Button btnSkipRoute;
    private boolean isChaperoneAbsented;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PickDropListStudentPMFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PickDropListStudentFragment newInstance(int columnCount) {
        PickDropListStudentFragment fragment = new PickDropListStudentFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
//        }
    }

    Manifest manifest;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pickup_student_list_pm, container, false);

        manifest = Constant.Manifests.get(0);
        adapter = new RecyclerAdapter();
        //adapter.setItems(manifest.ListStudents);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_point);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recyclerView.setAdapter(adapter);

        layoutChaperone = (CardView) view.findViewById(R.id.layout_chaperone);
        txtChapName = (TextView) view.findViewById(R.id.txt_chap_name);
        btnPresentChap = (Button) view.findViewById(R.id.btn_chaperone_present);
        btnAbsentChap = (Button) view.findViewById(R.id.btn_chaperone_absent);
        btnDepart = (Button) view.findViewById(R.id.btn_depart);

        if(manifest.IsCheckin)
            btnDepart.setVisibility(View.VISIBLE);
        else
            btnDepart.setVisibility(View.GONE);

        btnDepart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                depart();
            }
        });

        isChaperoneAbsented = false;
        btnPresentChap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isChaperoneAbsented = true;
                adjustBtnAbsentPresent(true);
                manifest.ChaperoneSkipTime = null;
                manifest.ChaperoneOnboardTime = new Date();
            }
        });

        btnAbsentChap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isChaperoneAbsented = true;
                adjustBtnAbsentPresent(false);
                manifest.ChaperoneSkipTime = new Date();
                manifest.ChaperoneOnboardTime = null;
            }
        });

        btnAbsentChap.setEnabled(false);
        btnPresentChap.setEnabled(false);

        return view;
    }

    private boolean validFinish() {
        boolean isValid = true;
        if(Global.CURRENT_POINT_INDEX > 0 && Global.CURRENT_POINT_INDEX <= manifest.ManifestItems.size())
        {
            ManifestItem manifestItem = manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1);
            for (int i = 0; i < manifestItem.ManifestItemDetails.size(); i++) {
                ManifestItemDetail mid = manifestItem.ManifestItemDetails.get(i);
                if(mid.PickDropTime == null && mid.SkipTime == null && mid.PlannedSkipTime == null)
                {
                    isValid = false;
                    break;
                }
            }

            if(!isValid) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
                // Add the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

                builder.setCancelable(false);
                builder.setMessage("Anda belum mengisi daftar hadir siswa!")
                        .setTitle("Pesan");

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }

        return isValid;
    }

    public void depart()
    {
        if(btnDepart.getText().toString().equals("FINISH"))
        {
            if(validFinish()) {
                ((OrderPMActivity) getActivity()).manifestItemDetailAction();
                //manifest.IsCheckin = false;
                //((OrderPMActivity) getActivity()).goToNextDestination();
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
                builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        Global.CURRENT_POINT_INDEX++;
                        while (Global.CURRENT_POINT_INDEX <= manifest.ManifestItems.size())
                        {
                            ((OrderPMActivity) getActivity()).manifestItemDetailAction();
                            Global.CURRENT_POINT_INDEX++;
                        }
                        ((OrderPMActivity) getActivity()).manifestFinish();
                        ((OrderPMActivity) getActivity()).showCloseOrderDialog();
                    }
                });
                builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

                builder.setCancelable(false);
                builder.setMessage("Anda yakin akan melakukan finish?")
                        .setTitle("Konfirmasi");

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
        else {
//            btnDepart.setEnabled(false);
//            GradientDrawable drawable = (GradientDrawable) btnDepart.getBackground();
//            drawable.setColor(Color.parseColor("#664abb47"));
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
//            // Add the buttons
//            builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//                    manifest.IsCheckin = false;
//                    ((OrderPMActivity) getActivity()).goToNextDestination();
//                }
//            });
//            builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//                    btnDepart.setEnabled(true);
//                    GradientDrawable drawable = (GradientDrawable) btnDepart.getBackground();
//                    drawable.setColor(Color.parseColor("#4abb47"));
//                }
//            });
//
//            builder.setCancelable(false);
//            builder.setMessage("Anda yakin akan menuju ke tujuan berikutnya?")
//                    .setTitle("Konfirmasi");
//
//            AlertDialog dialog = builder.create();
//            dialog.show();

            if(Global.CURRENT_POINT_INDEX == 0) // pickup chaperone
            {
                if(!isChaperoneAbsented)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
                    // Add the buttons
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });

                    builder.setCancelable(false);
                    builder.setMessage("Anda belum mengisi daftar hadir chaperone!")
                            .setTitle("Pesan");

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else {
                    btnDepart.setEnabled(false);
                    GradientDrawable drawable = (GradientDrawable) btnDepart.getBackground();
                    drawable.setColor(Color.parseColor("#664abb47"));

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
                    // Add the buttons
                    builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            manifest.IsCheckin = false;
                            ((OrderPMActivity) getActivity()).goToNextDestination();
                        }
                    });
                    builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            btnDepart.setEnabled(true);
                            GradientDrawable drawable = (GradientDrawable) btnDepart.getBackground();
                            drawable.setColor(Color.parseColor("#4abb47"));
                        }
                    });

                    builder.setCancelable(false);
                    builder.setMessage("Anda yakin akan menuju ke tujuan berikutnya?")
                            .setTitle("Konfirmasi");

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                return;
            }


            if(validDepart()) { // pickup students
                btnDepart.setEnabled(false);
                GradientDrawable drawable = (GradientDrawable) btnDepart.getBackground();
                drawable.setColor(Color.parseColor("#664abb47"));

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
                // Add the buttons
                builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        manifest.IsCheckin = false;
                        ((OrderPMActivity) getActivity()).goToNextDestination();
                    }
                });
                builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        btnDepart.setEnabled(true);
                        GradientDrawable drawable = (GradientDrawable) btnDepart.getBackground();
                        drawable.setColor(Color.parseColor("#4abb47"));
                    }
                });

                builder.setCancelable(false);
                builder.setMessage("Anda yakin akan menuju ke tujuan berikutnya?")
                        .setTitle("Konfirmasi");

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }

    private boolean validDepart() {
        boolean isValid = true;
        if(Global.CURRENT_POINT_INDEX > 0 && Global.CURRENT_POINT_INDEX <= manifest.ManifestItems.size())
        {
            ManifestItem manifestItem = manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1);
            for (int i = 0; i < manifestItem.ManifestItemDetails.size(); i++) {
                ManifestItemDetail mid = manifestItem.ManifestItemDetails.get(i);
                if(mid.PickDropTime == null && mid.SkipTime == null && mid.PlannedSkipTime == null)
                {
                    isValid = false;
                    break;
                }
            }

            if(!isValid) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
                // Add the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

                builder.setCancelable(false);
                builder.setMessage("Anda belum mengisi daftar hadir siswa!")
                        .setTitle("Pesan");

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }

        return isValid;
    }

    private void adjustBtnAbsentPresent(boolean isPresent) {
        if (isPresent) {
            btnPresentChap.setBackgroundResource(R.drawable.btn_present_pressed);
            btnAbsentChap.setBackgroundResource(R.drawable.btn_absent_not_pressed);
        } else
        {
            btnPresentChap.setBackgroundResource(R.drawable.btn_present_not_pressed);
            btnAbsentChap.setBackgroundResource(R.drawable.btn_absent_pressed);
        }

    }

    private int manifestItemType = 0;
    public void loadData(Manifest manifest) {
        this.manifest = manifest;
        if(Global.CURRENT_POINT_INDEX == 0)
        {
            layoutChaperone.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            txtChapName.setText(manifest.ChaperoneName);
        }
        else
        {
            layoutChaperone.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            if(manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX - 1).ManifestItemType == 2)
            {
                adapter.setItems(new ArrayList<ManifestItemDetail>());
            }
            else {
                // if gate 2, pickup
                if(manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX - 1).ManifestItemType == Constant.MANIFEST_ITEM_TYPE_DROP)
                {
                    manifestItemType = Constant.MANIFEST_ITEM_TYPE_DROP;
                    List<ManifestItemDetail> mids = manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX - 1).ManifestItemDetails;
                    for (int i = 0; i < manifest.ManifestItems.size(); i++) {
                        if(manifest.ManifestItems.get(i).ManifestItemType == Constant.MANIFEST_ITEM_TYPE_CHECKPOINT) break;
                        for (int j = 0; j < manifest.ManifestItems.get(i).ManifestItemDetails.size(); j++) {
                            for (int k = 0; k < mids.size(); k++) {
                                if(manifest.ManifestItems.get(i).ManifestItemDetails.get(j).MemberId == mids.get(k).MemberId)
                                {
                                    mids.get(k).DriverPlannedSkipTime = manifest.ManifestItems.get(i).ManifestItemDetails.get(j).DriverPlannedSkipTime;
                                    mids.get(k).SkipTime = manifest.ManifestItems.get(i).ManifestItemDetails.get(j).SkipTime;
                                    mids.get(k).isOnboard = mids.get(k).SkipTime == null && mids.get(k).DriverPlannedSkipTime == null;
                                }
                            }
                        }
                    }
                    adapter.setItems(mids);
                }
                else {
                    manifestItemType = Constant.MANIFEST_ITEM_TYPE_PICKUP;
                    List<ManifestItemDetail> mids = manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX - 1).ManifestItemDetails;
                    for (int i = 0; i < manifest.ManifestItems.size(); i++) {
                        if(manifest.ManifestItems.get(i).ManifestItemType == Constant.MANIFEST_ITEM_TYPE_CHECKPOINT) break;
                        for (int j = 0; j < manifest.ManifestItems.get(i).ManifestItemDetails.size(); j++) {
                            for (int k = 0; k < mids.size(); k++) {
                                if(manifest.ManifestItems.get(i).ManifestItemDetails.get(j).MemberId == mids.get(k).MemberId)
                                {
                                    mids.get(k).DriverPlannedSkipTime = manifest.ManifestItems.get(i).ManifestItemDetails.get(j).DriverPlannedSkipTime;
                                    mids.get(k).SkipTime = manifest.ManifestItems.get(i).ManifestItemDetails.get(j).SkipTime;
                                    mids.get(k).isOnboard = mids.get(k).SkipTime == null && mids.get(k).DriverPlannedSkipTime == null;
                                    if(mids.get(k).DriverPlannedSkipTime != null)
                                        mids.remove(k);
                                }
                            }
                        }
                    }
                    adapter.setItems(manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX - 1).ManifestItemDetails);
                }
            }

            adapter.notifyDataSetChanged();
        }

        if(this.manifest.IsCheckin)
        {
            if(Global.CURRENT_POINT_INDEX == 0)
            {
                btnAbsentChap.setEnabled(true);
                btnPresentChap.setEnabled(true);
            }
            else
            {
                recyclerView.setEnabled(true);
            }
            btnDepart.setVisibility(View.VISIBLE);
            btnDepart.setEnabled(true);
            if(Global.CURRENT_POINT_INDEX == manifest.ManifestItems.size())
            {
                GradientDrawable drawable = (GradientDrawable) btnDepart.getBackground();
                drawable.setColor(Color.parseColor("#551A8B"));
                btnDepart.setText("FINISH");
            }
            else {
                if (Global.CURRENT_POINT_INDEX > 0 && manifest.checkIfLastPoint(Global.CURRENT_POINT_INDEX, manifest.ManifestItems.get(0))) {
                    GradientDrawable drawable = (GradientDrawable) btnDepart.getBackground();
                    drawable.setColor(Color.parseColor("#551A8B"));
                    btnDepart.setText("FINISH");
                } else {
                    GradientDrawable drawable = (GradientDrawable) btnDepart.getBackground();
                    drawable.setColor(Color.parseColor("#4abb47"));
                }
            }
        }
        else
        {
            recyclerView.setEnabled(false);
            btnDepart.setVisibility(View.VISIBLE);
            btnDepart.setEnabled(false);
            //btnDepart.setVisibility(View.GONE);
            GradientDrawable drawable = (GradientDrawable) btnDepart.getBackground();
            drawable.setColor(Color.parseColor("#664abb47"));
            if(Global.CURRENT_POINT_INDEX == manifest.ManifestItems.size())
            {
                drawable.setColor(Color.parseColor("#66551A8B"));
                btnDepart.setText("FINISH");
            }
        }
    }

    public void refresh() {
        adapter.notifyDataSetChanged();
    }

    class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnTouchListener {
        private List<ManifestItemDetail> items;

        public RecyclerAdapter()
        {
            //this.items = new ArrayList<>();
        }

        public void setItems(List<ManifestItemDetail> items)
        {
            if(this.items == null)
                this.items = new ArrayList<>();

            this.items.clear();
            this.items.addAll(items);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_pickup_student, parent, false);
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            ItemViewHolder iv = (ItemViewHolder) holder;
            final ManifestItemDetail item = items.get(position);
            iv.txtName.setText(item.MemberName);
            iv.txtAddress.setText(item.MemberDetail);
            iv.txtInstruction.setText(item.Instruction);

            if (item.SkipTime != null || item.PlannedSkipTime != null) {
                iv.btnPresent.setBackgroundResource(R.drawable.btn_present_not_pressed);
                iv.btnAbsent.setBackgroundResource(R.drawable.btn_absent_pressed);
            }
            else if (item.PickDropTime != null) {
                iv.btnPresent.setBackgroundResource(R.drawable.btn_present_pressed);
                iv.btnAbsent.setBackgroundResource(R.drawable.btn_absent_not_pressed);
            }
            else
            {
                iv.btnPresent.setBackgroundResource(R.drawable.btn_present_not_pressed);
                iv.btnAbsent.setBackgroundResource(R.drawable.btn_absent_not_pressed);
            }

            if(manifest.IsCheckin)
            {
                iv.btnPresent.setEnabled(true);
                iv.btnAbsent.setEnabled(true);
            }
            else {
                iv.btnPresent.setEnabled(false);
                iv.btnAbsent.setEnabled(false);
            }

            iv.btnPresent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(item.PickDropTime != null)return;
                    item.isSkip = false;
                    item.SkipTime = null;
                    item.PickDropTime = new Date();
                    ((OrderPMActivity) getActivity()).updateCount();
                    notifyItemChanged(position);
                    ((OrderPMActivity) getActivity()).manifestItemDetailAction(item);
                }
            });

            iv.btnAbsent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(item.SkipTime != null) return;
                    item.isSkip = true;
                    item.SkipTime = new Date();
                    item.PickDropTime = null;
                    ((OrderPMActivity) getActivity()).updateCount();
                    notifyItemChanged(position);
                    ((OrderPMActivity) getActivity()).manifestItemDetailAction(item);
                }
            });

            if(position == getItemCount()-1) {
                // If so, add some margin to the element
                iv.setBottomMargin(R.dimen.last_item_offset); // some dimension (or set a fixed amount of pixels
            }
            else
            {
                iv.setBottomMargin(R.dimen.default_item_offset);
            }

            if(manifestItemType == Constant.MANIFEST_ITEM_TYPE_DROP)
            {
                if(item.isOnboard) {
                    iv.btnPresent.setEnabled(true);
                    iv.btnAbsent.setEnabled(false);
                }
                else
                {
                    iv.btnPresent.setEnabled(false);
                    iv.btnAbsent.setEnabled(false);
                }
            }
            else
            {
                if(item.DriverPlannedSkipTime != null || item.PlannedSkipTime != null) {
                    iv.btnPresent.setEnabled(false);
                    iv.btnAbsent.setEnabled(false);
                }
                else
                {
                    iv.btnPresent.setEnabled(true);
                    iv.btnAbsent.setEnabled(true);
                }
            }

//            if(item.isSkip)
//                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_pressed);
//            else
//                iv.btnSkip.setBackgroundResource(R.drawable.btn_skip_not_pressed);

//            final MenuItem item = items.get(position);
//
//            ((ItemViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    showDialog(100);
//                }
//            });
//            Picasso.with(PromoListActivity.this)
//                    .load(item.ImgUrl.replace(" ", "%20"))
////                            //.placeholder((position - HEADER_COUNT) % 2 == 0 ? R.drawable.default_image_large : R.drawable.default_image_small)
////                            //.error(R.drawable.flag_id)
//                    .into(((ItemViewHolder) holder).promo);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        class ItemViewHolder extends RecyclerView.ViewHolder {
            public TextView txtNo;
            public TextView txtName;
            public TextView txtAddress;
            public TextView txtInstruction;
            public Button btnPresent;
            public Button btnAbsent;
            private View mRootView;

            public ItemViewHolder(View itemView) {
                super(itemView);
                mRootView = itemView;
                txtNo  = (TextView) itemView.findViewById(R.id.item_no);
                txtName  = (TextView) itemView.findViewById(R.id.item_name);
                txtAddress  = (TextView) itemView.findViewById(R.id.item_address);
                txtInstruction  = (TextView) itemView.findViewById(R.id.item_instruction);
                btnPresent = (Button) itemView.findViewById(R.id.btn_present);
                btnAbsent = (Button) itemView.findViewById(R.id.btn_absent);
            }

            public void setBottomMargin(@DimenRes int margin){
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) mRootView.getLayoutParams();
                layoutParams.bottomMargin = (int)getContext().getResources().getDimension(margin);
                mRootView.setLayoutParams(layoutParams);
            }
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }


//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnListFragmentInteractionListener) {
//            mListener = (OnListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnListFragmentInteractionListener");
//        }
//    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction();
    }
}
