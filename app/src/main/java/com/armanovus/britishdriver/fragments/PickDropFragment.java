package com.armanovus.britishdriver.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.armanovus.britishdriver.Constant;
import com.armanovus.britishdriver.Global;
import com.armanovus.britishdriver.R;
import com.armanovus.britishdriver.models.Manifest;
import com.armanovus.britishdriver.models.ManifestItem;
import com.armanovus.britishdriver.util.Utility;
import com.armanovus.britishdriver.views.OrderActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class PickDropFragment extends android.support.v4.app.Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PickDropFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PickDropFragment newInstance(int columnCount) {
        PickDropFragment fragment = new PickDropFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
//        }
    }

    private TextView txtAddress;
    private TextView txtMaxTimeArrive;
    private TextView txtMaxTimeDepart;
    private Button btnMap;
    private Button btnNotes;
    private Button btnArrive;
    Manifest manifest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pickup, container, false);
        txtAddress = (TextView) view.findViewById(R.id.item_address);
        txtMaxTimeArrive = (TextView) view.findViewById(R.id.item_time_arrive);
        txtMaxTimeDepart = (TextView) view.findViewById(R.id.item_time_depart);
        btnMap = (Button) view.findViewById(R.id.btn_map);
        btnNotes = (Button) view.findViewById(R.id.btn_notes);
        btnArrive = (Button) view.findViewById(R.id.btn_arrived);

        manifest = Constant.Manifests.get(0);
        btnArrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkIn();
            }
        });

        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Location loc = Global.GetLocation(getActivity());
                if(loc != null) {
                    double lat = Global.CURRENT_POINT_INDEX == 0 ? manifest.ChaperoneLatitude : manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1).Latitude;
                    double lng = Global.CURRENT_POINT_INDEX == 0 ? manifest.ChaperoneLongitude : manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX-1).Longitude;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?"
                            + "saddr=" + loc.getLatitude() + "," + loc.getLongitude()
                            + "&daddr=" + lat + "," + lng));

                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                }
                else{
                    Utility.showAlert(getActivity(),"Lokasi saat ini tidak dapat ditentukan.");
                }
            }
        });

        btnNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNotesDialog();
            }
        });

        if(Global.CURRENT_POINT_INDEX == 0) {
            txtAddress.setText(manifest.ChaperonePickupAddress);
            Calendar cal = Calendar.getInstance();
            cal.setTime(manifest.ChaperonePickupTime);
            cal.add(Calendar.MINUTE, -2);
            txtMaxTimeArrive.setText(Constant.getHourAndTime(cal.getTime()));
            cal.add(Calendar.MINUTE, 4);
            txtMaxTimeDepart.setText(Constant.getHourAndTime(cal.getTime()));
        }
        else
            loadData();

        return view;
    }

    private Button btnSend;
    private Button btnCancel;
    private EditText txtNotes;
    private Spinner spNoteTypes;
    private Dialog dialogNotes;
    private ArrayAdapter<String> arrayAdapter;
    public void openNotesDialog()
    {
        dialogNotes = new Dialog(getActivity());
        dialogNotes.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNotes.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogNotes.setContentView(R.layout.dialog_notes);
        //dialogNotes.setCancelable(false);

        spNoteTypes = (Spinner) dialogNotes.findViewById(R.id.sp_notes_type);
        txtNotes = (EditText) dialogNotes.findViewById(R.id.txt_notes);
        btnSend = (Button) dialogNotes.findViewById(R.id.btn_send);
        btnCancel = (Button) dialogNotes.findViewById(R.id.btn_cancel);

        List<String> noteTypes = new ArrayList<>();
        for (int i = 0; i < Global.NoteTypes.size(); i++) {
            noteTypes.add(Global.NoteTypes.get(i).Name);
        }
        arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, noteTypes);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_note);
        spNoteTypes.setAdapter(arrayAdapter);
        //spNoteTypes.setPrompt("");

        spNoteTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView selectedText = (TextView) parent.getChildAt(0);
                if (selectedText != null) {
                    selectedText.setTextColor(Color.parseColor("#ffffff"));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//                TextView selectedText = (TextView) parent.getChildAt(0);
//                if (selectedText != null) {
//                    selectedText.setTextColor(Color.parseColor("#ffffff"));
//                }
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hideSoftKeyboard();
                dialogNotes.dismiss();
                if(!TextUtils.isEmpty(txtNotes.getText()))
                    ((OrderActivity)getActivity()).sendNotes(txtNotes.getText().toString(), spNoteTypes.getSelectedItemPosition());
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogNotes.dismiss();
            }
        });

        dialogNotes.show();
    }

    public void setManifest(Manifest m)
    {
        this.manifest = m;
    }

    public void checkIn() {
        btnArrive.setEnabled(false);
        GradientDrawable drawable = (GradientDrawable) btnArrive.getBackground();
        drawable.setColor(Color.parseColor("#664abb47"));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
        // Add the buttons
        builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                manifest.IsCheckin = true;
                ((OrderActivity)getActivity()).goToPickupList();
            }
        });
        builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                btnArrive.setEnabled(true);
                GradientDrawable drawable = (GradientDrawable) btnArrive.getBackground();
                drawable.setColor(Color.parseColor("#4abb47"));
            }
        });

        builder.setCancelable(false);
        builder.setMessage("Anda yakin telah sampai di tujuan penjemputan?")
                .setTitle("Konfirmasi");

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void loadData() {
        if(Global.CURRENT_POINT_INDEX > 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX - 1).PickupTime);
            cal.add(Calendar.MINUTE, -2);
            txtAddress.setText(manifest.ManifestItems.get(Global.CURRENT_POINT_INDEX - 1).getAddressDetail());
            txtMaxTimeArrive.setText(Constant.getHourAndTime(cal.getTime()));
            cal.add(Calendar.MINUTE, 4);
            txtMaxTimeDepart.setText(Constant.getHourAndTime(cal.getTime()));
        }

        if(!manifest.IsCheckin) {
            btnArrive.setEnabled(true);
            GradientDrawable drawable = (GradientDrawable) btnArrive.getBackground();
            drawable.setColor(Color.parseColor("#4abb47"));
        }
        else
        {
            btnArrive.setEnabled(false);
            GradientDrawable drawable = (GradientDrawable) btnArrive.getBackground();
            drawable.setColor(Color.parseColor("#664abb47"));
        }
    }

    class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnTouchListener {
        private List<ManifestItem> items;

        public RecyclerAdapter()
        {
            //this.items = new ArrayList<>();
        }

        public void setItems(List<ManifestItem> items)
        {
            this.items = items;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_point, parent, false);
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            ItemViewHolder iv = (ItemViewHolder) holder;
            ManifestItem item = items.get(position);
            iv.txtNo.setText("" + (position + 1));
            iv.txtAddress.setText(item.getAddressDetail());
            iv.txtTime.setText(item.getPickupTime());
//            final MenuItem item = items.get(position);
//
//            ((ItemViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    showDialog(100);
//                }
//            });
//            Picasso.with(PromoListActivity.this)
//                    .load(item.ImgUrl.replace(" ", "%20"))
////                            //.placeholder((position - HEADER_COUNT) % 2 == 0 ? R.drawable.default_image_large : R.drawable.default_image_small)
////                            //.error(R.drawable.flag_id)
//                    .into(((ItemViewHolder) holder).promo);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        class ItemViewHolder extends RecyclerView.ViewHolder {
            public TextView txtNo;
            public TextView txtAddress;
            public TextView txtTime;

            public ItemViewHolder(View itemView) {
                super(itemView);
                txtNo  = (TextView) itemView.findViewById(R.id.item_no);
                txtAddress  = (TextView) itemView.findViewById(R.id.item_address);
                txtTime  = (TextView) itemView.findViewById(R.id.item_time);
            }
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }


//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnListFragmentInteractionListener) {
//            mListener = (OnListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnListFragmentInteractionListener");
//        }
//    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction();
    }
}
